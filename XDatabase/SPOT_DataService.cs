﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NLog;

namespace XDatabase
{
    public static class SPOT_DataService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static string ProcessDataConnectionString = "";

        public static void InitProcessDataConnectionString(string DataSource, string InitialCatalog, string UserID, string DecryptedPassword)
        {
            SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder.ApplicationName = Assembly.GetExecutingAssembly().GetName().Name;
            builder.DataSource = DataSource;
            builder.InitialCatalog = InitialCatalog;
            builder.UserID = UserID;
            builder.Password = DecryptedPassword;
            ProcessDataConnectionString = builder.ConnectionString;
        }



        public async static Task<string> TestTask()
        {
            string result = "NoResult";

            try
            {
                logger.Info("entering {0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);

                result = await XDatabase.SQLUtil.GetXMLNodeFromStoredProcedure(ProcessDataConnectionString, "SPOT_Task", "Test", "Result", null);
            }

            catch (Exception ex)
            {
                logger.Error(ex);
                result = ex.Message;
            }

            finally
            {
                logger.Info("exiting  {0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }

        public async static Task<StampGeometry> GetStampGeometryBySerialNumber(string serialNumber)
        {
            StampGeometry stampGeometry = null;

            try
            {
                logger.Info("entering {0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);

                var queryParameters = new List<KeyValuePair<string, string>>()
                    {
                        new KeyValuePair<string, string>("SerialNumber", serialNumber ),
                    };

                string sStampGeometry = await SQLUtil.GetXMLNodeFromStoredProcedure(ProcessDataConnectionString , "SPOT_Task", "GetStampGeometryBySerialNumber", "StampGeometry", queryParameters);

                XmlSerializer deserializer = new XmlSerializer(typeof(StampGeometry), new XmlRootAttribute("StampGeometry"));

                using (TextReader reader = new StringReader(sStampGeometry))
                {
                    stampGeometry = (StampGeometry)deserializer.Deserialize(reader);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                logger.Info("exiting  {0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
            }

            return stampGeometry;
        }


    }

    //private void ProcessXMLOutputFiles(XmlDocument doc)
    //{
    //    int iFileCount = 1;

    //    try
    //    {
    //        logger.Info("entering {0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);

    //        //XMLOutputFilePaths  XMLOutputFileContents
    //        XmlElement root = doc.DocumentElement;
    //        XmlNode outputFilesNode = root.SelectSingleNode("OutputFiles"); // You can also use XPath here

    //        foreach (XmlNode outputFileNode in outputFilesNode)
    //        {
    //            if (outputFileNode.Name == "OutputFile")
    //            {
    //                Guid g = Guid.NewGuid();
    //                string fileName = g + ".xml";

    //                // MessageBox.Show(filePathNode.InnerText);
    //                // MessageBox.Show(outputFileContents.InnerXml);
    //                XmlDocument xmlOutputDoc = new XmlDocument();
    //                xmlOutputDoc.LoadXml(outputFileNode.InnerXml);
    //                xmlOutputDoc.Save(programConfig.OutputXMLFilePath + @"\" + fileName);
    //            }

    //        }

    //    }

    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }

    //    finally
    //    {
    //        logger.Info("exiting  {0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
    //    }


    //}

    //private void ProcessXMLMessages(XmlDocument doc)
    //{
    //    string messageText = "";
    //    string messageTitle = "";
    //    string messageType = "";

    //    try
    //    {
    //        logger.Info("entering {0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);

    //        //XMLOutputFilePaths  XMLOutputFileContents
    //        XmlElement root = doc.DocumentElement;
    //        XmlNode uiMessageNode = root.SelectSingleNode("UIMessage"); // You can also use XPath here

    //        foreach (XmlNode uiMessageSubNode in uiMessageNode)
    //        {
    //            switch (uiMessageSubNode.Name.ToUpper())
    //            {
    //                case "MESSAGETEXT":
    //                    messageText = uiMessageSubNode.InnerText;
    //                    break;
    //                case "MESSAGETITLE":
    //                    messageTitle = uiMessageSubNode.InnerText;
    //                    break;
    //                case "MESSAGETYPE":
    //                    messageType = uiMessageSubNode.InnerText;
    //                    break;
    //                default:
    //                    break;
    //            }
    //        }


    //        if (messageText.Length > 0)
    //        {
    //            if (messageType.ToUpper() == "STATUS")
    //            {
    //                lblStatus.Text = messageText;
    //            }
    //            else
    //            {
    //                if (messageTitle.Length == 0)
    //                {
    //                    messageTitle = "UIMessage from Database";
    //                }
    //                MessageBox.Show(messageText, messageTitle);
    //            }
    //        }



    //    }

    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }

    //    finally
    //    {
    //        logger.Info("exiting  {0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name);
    //    }


    //}

}
