﻿using System.Xml.Serialization;

namespace XDatabase
{
    [XmlType("StampGeometry")]
    public class StampGeometry : XMLBaseObject
    {
        public string XDCStampID { get; set; } = "";
        public string SerialNumber { get; set; } = "";
        public string FailureMessage { get; set; } = "";
        public string StampMasterLotID { get; set; } = "";
        public int ArraySizeX { get; set; } = 0;
        public int ArraySizeY { get; set; } = 0;
        public decimal PitchX { get; set; } = 0;
        public decimal PitchY { get; set; } = 0;
        public decimal PedestalThickness { get; set; } = 0;
        public decimal MesaHeight { get; set; } = 0;
        public decimal SuperPostHeight { get; set; } = 0;
        public decimal PostHeight { get; set; } = 0;
        public decimal ApertureDiameter { get; set; } = 0;
        public decimal MesaWidth { get; set; } = 0;
        public decimal MesaLength { get; set; } = 0;
        public decimal MesaBorderX { get; set; } = 0;
        public decimal MesaBorderY { get; set; } = 0;
        public decimal PostPitchX { get; set; } = 0;
        public decimal PostPitchY { get; set; } = 0;
        public decimal SuperPostWidth { get; set; } = 0;
        public decimal SuperPostLength { get; set; } = 0;
        public decimal PostWidth { get; set; } = 0;
        public decimal PostLength { get; set; } = 0;
        public string Comments { get; set; } = "";

        /// <summary>
        /// Parameterless instantiation method required for serialization.
        /// </summary>
        public StampGeometry() { }
    }
}