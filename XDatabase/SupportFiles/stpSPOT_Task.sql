/****** Object:  StoredProcedure [dbo].[ProcessTask]    Script Date: 2/9/2021 3:20:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO













ALTER PROCEDURE [dbo].[SPOT_Task] 
 @XMLInputParameters XML,
 @XMLOutputParameters XML OUTPUT, 
 @debug AS BIT = 0
AS

/*
Sample Exe Script:
--DECLARE @XMLInputParameters XML
--DECLARE @XMLOutputParameters XML
--SET @XMLInputParameters ='<XMLInputParameters><Task>Test</Task></XMLInputParameters>'

--EXEC [ProcessTask] @XMLInputParameters = @XMLInputParameters, @XMLOutputParameters = @XMLOutputParameters OUTPUT, @debug = 1

--SELECT @XMLOutputParameters 

-- Permissions
GRANT EXECUTE ON [dbo].[ProcessTask] TO [ProcessData_User] AS [dbo]
 

-- Change History
Developer       Date            Changes
Ed Hubbell      2020/11/03      Created procedure.

*/


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

DECLARE @LogID BIGINT
DECLARE @Task VARCHAR(100)
DECLARE @LotID VARCHAR(100)
DECLARE @ComputerName VARCHAR(100)
DECLARE @UserName VARCHAR(100)
DECLARE @StartDateTimeOffset DATETIMEOFFSET

DECLARE @Result VARCHAR(10) = '';

DECLARE @SerialNumber VARCHAR(50)
DECLARE @SerialNumberCount INT

DECLARE @XDCStampID VARCHAR(50)
DECLARE @StampMasterLotID VARCHAR(50)

DECLARE @StampGeometry XML


DECLARE @FailureMessage VARCHAR(200) = '';


BEGIN TRY

  IF @debug = 1 SELECT @XMLInputParameters

  SET @StartDateTimeOffset = dbo.DATETIMEOFFSET_EST()

  -- Log exe of the stored procedure.
  INSERT INTO SPOT_Task_Log (TransactionTime) VALUES (@StartDateTimeOffset) 
  SET @LogID = @@IDENTITY 

  UPDATE SPOT_Task_Log SET XMLInputParameters = @XMLInputParameters WHERE LogID = @LogID 

  -- Keep only the last 10K records.
  IF (SELECT COUNT(*) FROM SPOT_Task_Log) > 11000
    BEGIN
      DELETE FROM SPOT_Task_Log WHERE LogID < @LogID - 10000 
    END 

  SELECT @Task = ISNULL(DataRows.ID.value(N'(Task/text())[1]','VARCHAR(100)'), 'NotSentInXML'), 
    @ComputerName = ISNULL(DataRows.ID.value(N'(ComputerName/text())[1]','VARCHAR(100)'), 'NotSentInXML'),
    @UserName = ISNULL(DataRows.ID.value(N'(UserName/text())[1]','VARCHAR(100)'), 'NotSentInXML')
    FROM @XMLInputParameters.nodes('./XMLInputParameters') AS DataRows(ID)


  IF @Task = 'Test'
    BEGIN
       -- Delay for 2 seconds.
      WAITFOR DELAY '00:00:02';

      SET @XMLOutputParameters = (
       SELECT @Task AS Task, 'SPOT_Task_Test_Success' AS Result
       FOR XML RAW ('XMLOutputParameters'), ELEMENTS)
    END 


  --IF @Task LIKE 'LEDQA%'
  --  BEGIN
  --    -- Shell out to the LEDQA Task stored procedure. That's going to have some complex logic in it, and is as good a point as any to stop piling into this single stored procedure. 
  --    EXEC [LEDQA_Task] @XMLInputParameters = @XMLInputParameters, @XMLOutputParameters = @XMLOutputParameters OUTPUT, @debug = @debug
  --  END


  IF @Task = 'GetStampGeometryBySerialNumber'
    BEGIN
      SELECT @SerialNumber = ISNULL(DataRows.ID.value(N'(SerialNumber/text())[1]','NVARCHAR(50)'), 'NotSentInXML')
        FROM @XMLInputParameters.nodes('./XMLInputParameters') AS DataRows(ID)   


      SET @SerialNumberCount = (SELECT COUNT(*) FROM [dbo].[StampInventory] WHERE SerialNumber = @SerialNumber AND TransactionIsLatest = 1)

      -- If there is more than one record, or no records, of this serial number, then we need to tell the user as much.  
      IF (@SerialNumberCount = 0) SET @FailureMessage = 'Error - 0 records for specified Stamp SerialNumber (' + @SerialNumber +') in the StampInventory table. Use XData app to address this.'
      IF (@SerialNumberCount > 1) SET @FailureMessage = 'Error - More than 1 record for specified Stamp SerialNumber (' + @SerialNumber + ') in the StampInventory table. Use XData app to address this.'

      IF (LEN(@FailureMessage) = 0)
        BEGIN 

          SELECT TOP 1 @StampMasterLotID = StampMasterLotID, @XDCStampID = XDCStampID FROM [dbo].[StampInventory] WHERE SerialNumber = @SerialNumber AND TransactionIsLatest = 1

          SET @StampGeometry = (
            SELECT SMG.*, @SerialNumber AS SerialNumber, @StampMasterLotID AS StampMasterLotID, @XDCStampID AS XDCStampID 
              FROM [dbo].[StampMasterGeometry] SMG
              WHERE SMG.TransactionIsLatest = 1
              AND SMG.StampMasterLotID = @StampMasterLotID
              FOR XML RAW ('StampGeometry'), ELEMENTS, TYPE) 

          IF (@StampGeometry IS NULL) SET @FailureMessage = 'Error - No records for the StampMasterLotID (' + @StampMasterLotID + ') associated with the provided Stamp SerialNumber (' + @SerialNumber + ') exist in the StampMasterGeometry table. Use XData app to address this.'

        END

      IF (LEN(@FailureMessage) = 0)
        BEGIN 
          SET @XMLOutputParameters = (
            SELECT 'Success' AS Result, @StampGeometry
              FOR XML RAW ('XMLOutputParameters'), ELEMENTS)
        END
      ELSE 
        BEGIN
          SET @XMLOutputParameters = (
            SELECT 'Failure' AS Result, (SELECT @FailureMessage AS FailureMessage FOR XML RAW ('StampGeometry'), ELEMENTS, TYPE)
              FOR XML RAW ('XMLOutputParameters'), ELEMENTS)
        END


    END
 



  -- Finally update the task log with the resulting XMLOutputParameters that is sent back to the calling application.
  UPDATE SPOT_Task_Log 
    SET XMLOutputParameters = @XMLOutputParameters,
      Duration_ms = DATEDIFF(ms, @StartDateTimeOffset, dbo.DATETIMEOFFSET_EST())
    WHERE LogID = @LogID

  RETURN

END TRY

BEGIN CATCH

  DECLARE @CErrMsg VARCHAR(MAX) 
  DECLARE @CErrNum INT 
  DECLARE @CErrSeverity INT 
  DECLARE @CErrState INT
  DECLARE @CErrLine INT

  SELECT @CErrMsg = ERROR_MESSAGE(), 
    @CErrNum = ERROR_NUMBER(), 
    @CErrSeverity = ERROR_SEVERITY(), 
    @CErrState = ERROR_STATE(), 
    @CErrLine = ERROR_LINE()


  SET @CErrMsg = 'Proc: ' + OBJECT_NAME(@@ProcID) + ', Line: ' + CAST(@CErrLine as varchar(20)) + ', ErrNum: ' + CAST(@CErrNum as varchar(20)) + ' : ' + @CErrMsg

  SET @XMLOutputParameters = (
    SELECT @CErrMsg AS CErrMsg, 'Error' AS Result
    FOR XML RAW ('XMLOutputParameters'), ELEMENTS)

  RAISERROR(@CErrMsg, @CErrSeverity, @CErrState)

  RETURN

END CATCH
GO


