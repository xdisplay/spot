﻿
using System;

namespace SPOT
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.NumZoom = new System.Windows.Forms.NumericUpDown();
            this.tlbPosition = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblIntensity = new System.Windows.Forms.Label();
            this.lblH = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.lblZ = new System.Windows.Forms.Label();
            this.lblXpos = new System.Windows.Forms.Label();
            this.lblYpos = new System.Windows.Forms.Label();
            this.lblZpos = new System.Windows.Forms.Label();
            this.flowMotionHotkeys = new System.Windows.Forms.FlowLayoutPanel();
            this.btnHomeAllStages = new System.Windows.Forms.Button();
            this.btnToCenter = new System.Windows.Forms.Button();
            this.btnToLoad = new System.Windows.Forms.Button();
            this.pbxVision = new System.Windows.Forms.PictureBox();
            this.plotView = new OxyPlot.WindowsForms.PlotView();
            this.label1 = new System.Windows.Forms.Label();
            this.NumWhiteLED = new System.Windows.Forms.NumericUpDown();
            this.NumRedLED = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonSwitchFeed = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonShowZoom = new System.Windows.Forms.ToolStripButton();
            this.toolsStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.stampScanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.waferScanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mmToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.mmToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.mmToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.mmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mmToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mmToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.shuttleToSensorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shuttleToCameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scanParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportPlotDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calibrateSidecamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archiveLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetPlotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SEYRtoolStripButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripButtonRegistration = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOpenCompass = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonReset = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.tlpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumZoom)).BeginInit();
            this.tlbPosition.SuspendLayout();
            this.flowMotionHotkeys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumWhiteLED)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRedLED)).BeginInit();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.AutoSize = true;
            this.tlpMain.ColumnCount = 5;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpMain.Controls.Add(this.label3, 0, 5);
            this.tlpMain.Controls.Add(this.NumZoom, 1, 5);
            this.tlpMain.Controls.Add(this.tlbPosition, 0, 2);
            this.tlpMain.Controls.Add(this.flowMotionHotkeys, 1, 2);
            this.tlpMain.Controls.Add(this.pbxVision, 3, 1);
            this.tlpMain.Controls.Add(this.plotView, 4, 1);
            this.tlpMain.Controls.Add(this.label1, 0, 3);
            this.tlpMain.Controls.Add(this.NumWhiteLED, 1, 3);
            this.tlpMain.Controls.Add(this.NumRedLED, 1, 4);
            this.tlpMain.Controls.Add(this.label4, 0, 4);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 7;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 260F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Size = new System.Drawing.Size(1094, 486);
            this.tlpMain.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 457);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Zoom %";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NumZoom
            // 
            this.NumZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.NumZoom.Location = new System.Drawing.Point(128, 454);
            this.NumZoom.Name = "NumZoom";
            this.NumZoom.Size = new System.Drawing.Size(74, 20);
            this.NumZoom.TabIndex = 11;
            this.NumZoom.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.NumZoom.ValueChanged += new System.EventHandler(this.NumZoom_ValueChanged);
            // 
            // tlbPosition
            // 
            this.tlbPosition.AutoSize = true;
            this.tlbPosition.ColumnCount = 2;
            this.tlbPosition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlbPosition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlbPosition.Controls.Add(this.label2, 0, 4);
            this.tlbPosition.Controls.Add(this.lblIntensity, 0, 4);
            this.tlbPosition.Controls.Add(this.lblH, 0, 3);
            this.tlbPosition.Controls.Add(this.lblHeight, 1, 3);
            this.tlbPosition.Controls.Add(this.lblX, 0, 0);
            this.tlbPosition.Controls.Add(this.lblY, 0, 1);
            this.tlbPosition.Controls.Add(this.lblZ, 0, 2);
            this.tlbPosition.Controls.Add(this.lblXpos, 1, 0);
            this.tlbPosition.Controls.Add(this.lblYpos, 1, 1);
            this.tlbPosition.Controls.Add(this.lblZpos, 1, 2);
            this.tlbPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlbPosition.Location = new System.Drawing.Point(3, 293);
            this.tlbPosition.Name = "tlbPosition";
            this.tlbPosition.RowCount = 5;
            this.tlbPosition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlbPosition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlbPosition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlbPosition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlbPosition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlbPosition.Size = new System.Drawing.Size(119, 103);
            this.tlbPosition.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 23);
            this.label2.TabIndex = 8;
            this.label2.Text = "I (%)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIntensity
            // 
            this.lblIntensity.AutoSize = true;
            this.lblIntensity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIntensity.Location = new System.Drawing.Point(49, 80);
            this.lblIntensity.Name = "lblIntensity";
            this.lblIntensity.Size = new System.Drawing.Size(67, 23);
            this.lblIntensity.TabIndex = 7;
            this.lblIntensity.Text = "N/A";
            this.lblIntensity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblH
            // 
            this.lblH.AutoSize = true;
            this.lblH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblH.Location = new System.Drawing.Point(3, 60);
            this.lblH.Name = "lblH";
            this.lblH.Size = new System.Drawing.Size(40, 20);
            this.lblH.TabIndex = 6;
            this.lblH.Text = "H (mm)";
            this.lblH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeight.Location = new System.Drawing.Point(49, 60);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(67, 20);
            this.lblHeight.TabIndex = 1;
            this.lblHeight.Text = "N/A";
            this.lblHeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblX.Location = new System.Drawing.Point(3, 0);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(40, 20);
            this.lblX.TabIndex = 0;
            this.lblX.Text = "X (mm)";
            this.lblX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblY.Location = new System.Drawing.Point(3, 20);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(40, 20);
            this.lblY.TabIndex = 1;
            this.lblY.Text = "Y (mm)";
            this.lblY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblZ
            // 
            this.lblZ.AutoSize = true;
            this.lblZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZ.Location = new System.Drawing.Point(3, 40);
            this.lblZ.Name = "lblZ";
            this.lblZ.Size = new System.Drawing.Size(40, 20);
            this.lblZ.TabIndex = 2;
            this.lblZ.Text = "Z (mm)";
            this.lblZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblXpos
            // 
            this.lblXpos.AutoSize = true;
            this.lblXpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblXpos.Location = new System.Drawing.Point(49, 0);
            this.lblXpos.Name = "lblXpos";
            this.lblXpos.Size = new System.Drawing.Size(67, 20);
            this.lblXpos.TabIndex = 3;
            this.lblXpos.Text = "0.000";
            this.lblXpos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYpos
            // 
            this.lblYpos.AutoSize = true;
            this.lblYpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblYpos.Location = new System.Drawing.Point(49, 20);
            this.lblYpos.Name = "lblYpos";
            this.lblYpos.Size = new System.Drawing.Size(67, 20);
            this.lblYpos.TabIndex = 4;
            this.lblYpos.Text = "0.000";
            this.lblYpos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZpos
            // 
            this.lblZpos.AutoSize = true;
            this.lblZpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZpos.Location = new System.Drawing.Point(49, 40);
            this.lblZpos.Name = "lblZpos";
            this.lblZpos.Size = new System.Drawing.Size(67, 20);
            this.lblZpos.TabIndex = 5;
            this.lblZpos.Text = "0.000";
            this.lblZpos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowMotionHotkeys
            // 
            this.flowMotionHotkeys.AutoSize = true;
            this.flowMotionHotkeys.Controls.Add(this.btnHomeAllStages);
            this.flowMotionHotkeys.Controls.Add(this.btnToCenter);
            this.flowMotionHotkeys.Controls.Add(this.btnToLoad);
            this.flowMotionHotkeys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowMotionHotkeys.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowMotionHotkeys.Location = new System.Drawing.Point(128, 293);
            this.flowMotionHotkeys.Name = "flowMotionHotkeys";
            this.flowMotionHotkeys.Size = new System.Drawing.Size(74, 103);
            this.flowMotionHotkeys.TabIndex = 4;
            // 
            // btnHomeAllStages
            // 
            this.btnHomeAllStages.BackColor = System.Drawing.Color.White;
            this.btnHomeAllStages.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnHomeAllStages.FlatAppearance.BorderSize = 4;
            this.btnHomeAllStages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHomeAllStages.Location = new System.Drawing.Point(3, 3);
            this.btnHomeAllStages.Name = "btnHomeAllStages";
            this.btnHomeAllStages.Size = new System.Drawing.Size(71, 29);
            this.btnHomeAllStages.TabIndex = 2;
            this.btnHomeAllStages.Text = "Homed";
            this.btnHomeAllStages.UseVisualStyleBackColor = false;
            this.btnHomeAllStages.Click += new System.EventHandler(this.BtnHomeAllStages_Click);
            // 
            // btnToCenter
            // 
            this.btnToCenter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToCenter.AutoSize = true;
            this.btnToCenter.BackColor = System.Drawing.Color.White;
            this.btnToCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToCenter.Location = new System.Drawing.Point(3, 38);
            this.btnToCenter.Name = "btnToCenter";
            this.btnToCenter.Size = new System.Drawing.Size(71, 28);
            this.btnToCenter.TabIndex = 3;
            this.btnToCenter.Text = "To Center";
            this.btnToCenter.UseVisualStyleBackColor = false;
            this.btnToCenter.Click += new System.EventHandler(this.BtnToCenter_Click);
            // 
            // btnToLoad
            // 
            this.btnToLoad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToLoad.AutoSize = true;
            this.btnToLoad.BackColor = System.Drawing.Color.White;
            this.btnToLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToLoad.Location = new System.Drawing.Point(3, 72);
            this.btnToLoad.Name = "btnToLoad";
            this.btnToLoad.Size = new System.Drawing.Size(71, 28);
            this.btnToLoad.TabIndex = 4;
            this.btnToLoad.Text = "To Load";
            this.btnToLoad.UseVisualStyleBackColor = false;
            this.btnToLoad.Click += new System.EventHandler(this.btnToLoad_Click);
            // 
            // pbxVision
            // 
            this.pbxVision.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbxVision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxVision.Location = new System.Drawing.Point(283, 33);
            this.pbxVision.Name = "pbxVision";
            this.tlpMain.SetRowSpan(this.pbxVision, 6);
            this.pbxVision.Size = new System.Drawing.Size(401, 450);
            this.pbxVision.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxVision.TabIndex = 5;
            this.pbxVision.TabStop = false;
            this.pbxVision.Tag = "1";
            // 
            // plotView
            // 
            this.plotView.BackColor = System.Drawing.Color.Silver;
            this.plotView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plotView.Location = new System.Drawing.Point(690, 33);
            this.plotView.Name = "plotView";
            this.plotView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.tlpMain.SetRowSpan(this.plotView, 6);
            this.plotView.Size = new System.Drawing.Size(401, 450);
            this.plotView.TabIndex = 6;
            this.plotView.Text = "plotView1";
            this.plotView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plotView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plotView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 405);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "White LED %";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NumWhiteLED
            // 
            this.NumWhiteLED.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.NumWhiteLED.Location = new System.Drawing.Point(128, 402);
            this.NumWhiteLED.Name = "NumWhiteLED";
            this.NumWhiteLED.Size = new System.Drawing.Size(74, 20);
            this.NumWhiteLED.TabIndex = 9;
            this.NumWhiteLED.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.NumWhiteLED.ValueChanged += new System.EventHandler(this.NumWhiteLED_ValueChanged);
            // 
            // NumRedLED
            // 
            this.NumRedLED.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.NumRedLED.Location = new System.Drawing.Point(128, 428);
            this.NumRedLED.Name = "NumRedLED";
            this.NumRedLED.Size = new System.Drawing.Size(74, 20);
            this.NumRedLED.TabIndex = 13;
            this.NumRedLED.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.NumRedLED.ValueChanged += new System.EventHandler(this.NumRedLED_ValueChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 431);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Red LED %";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonSwitchFeed,
            this.toolStripButtonShowZoom,
            this.toolsStripDropDownButton,
            this.SEYRtoolStripButton,
            this.toolStripButtonRegistration,
            this.toolStripButtonOpenCompass,
            this.toolStripButtonReset,
            this.toolStripButtonHelp,
            this.toolStripProgressBar});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1094, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButtonSwitchFeed
            // 
            this.toolStripButtonSwitchFeed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSwitchFeed.Image = global::SPOT.Properties.Resources.iconmonstr_photo_camera_8_16;
            this.toolStripButtonSwitchFeed.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSwitchFeed.Name = "toolStripButtonSwitchFeed";
            this.toolStripButtonSwitchFeed.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSwitchFeed.Text = "Switch Camera Feed";
            this.toolStripButtonSwitchFeed.Click += new System.EventHandler(this.toolStripButtonSwitchFeed_Click);
            // 
            // toolStripButtonShowZoom
            // 
            this.toolStripButtonShowZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonShowZoom.Image = global::SPOT.Properties.Resources.iconmonstr_magnifier_7_24;
            this.toolStripButtonShowZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonShowZoom.Name = "toolStripButtonShowZoom";
            this.toolStripButtonShowZoom.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonShowZoom.Text = "Show Zoom";
            this.toolStripButtonShowZoom.Click += new System.EventHandler(this.toolStripButtonShowZoom_Click);
            // 
            // toolsStripDropDownButton
            // 
            this.toolsStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolsStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stampScanToolStripMenuItem,
            this.waferScanToolStripMenuItem,
            this.shuttleToSensorToolStripMenuItem,
            this.shuttleToCameraToolStripMenuItem,
            this.scanParametersToolStripMenuItem,
            this.exportPlotDataToolStripMenuItem,
            this.calibrateSidecamToolStripMenuItem,
            this.archiveLogsToolStripMenuItem,
            this.resetPlotToolStripMenuItem});
            this.toolsStripDropDownButton.Image = global::SPOT.Properties.Resources.iconmonstr_crop_8_24;
            this.toolsStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolsStripDropDownButton.Name = "toolsStripDropDownButton";
            this.toolsStripDropDownButton.Size = new System.Drawing.Size(29, 22);
            this.toolsStripDropDownButton.Text = "Tools";
            // 
            // stampScanToolStripMenuItem
            // 
            this.stampScanToolStripMenuItem.BackColor = System.Drawing.Color.LightBlue;
            this.stampScanToolStripMenuItem.Name = "stampScanToolStripMenuItem";
            this.stampScanToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.stampScanToolStripMenuItem.Text = "Stamp scan";
            this.stampScanToolStripMenuItem.Click += new System.EventHandler(this.stampScanToolStripMenuItem_Click);
            // 
            // waferScanToolStripMenuItem
            // 
            this.waferScanToolStripMenuItem.BackColor = System.Drawing.Color.LightBlue;
            this.waferScanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mmToolStripMenuItem3,
            this.mmToolStripMenuItem4,
            this.mmToolStripMenuItem5,
            this.mmToolStripMenuItem,
            this.mmToolStripMenuItem1,
            this.mmToolStripMenuItem2});
            this.waferScanToolStripMenuItem.Name = "waferScanToolStripMenuItem";
            this.waferScanToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.waferScanToolStripMenuItem.Text = "Wafer scan";
            // 
            // mmToolStripMenuItem3
            // 
            this.mmToolStripMenuItem3.Name = "mmToolStripMenuItem3";
            this.mmToolStripMenuItem3.Size = new System.Drawing.Size(114, 22);
            this.mmToolStripMenuItem3.Tag = "50";
            this.mmToolStripMenuItem3.Text = "50mm";
            this.mmToolStripMenuItem3.Click += new System.EventHandler(this.WaferScanToolStripMenuItem_Click);
            // 
            // mmToolStripMenuItem4
            // 
            this.mmToolStripMenuItem4.Name = "mmToolStripMenuItem4";
            this.mmToolStripMenuItem4.Size = new System.Drawing.Size(114, 22);
            this.mmToolStripMenuItem4.Tag = "60";
            this.mmToolStripMenuItem4.Text = "60mm";
            this.mmToolStripMenuItem4.Click += new System.EventHandler(this.WaferScanToolStripMenuItem_Click);
            // 
            // mmToolStripMenuItem5
            // 
            this.mmToolStripMenuItem5.Name = "mmToolStripMenuItem5";
            this.mmToolStripMenuItem5.Size = new System.Drawing.Size(114, 22);
            this.mmToolStripMenuItem5.Tag = "90";
            this.mmToolStripMenuItem5.Text = "90mm";
            this.mmToolStripMenuItem5.Click += new System.EventHandler(this.WaferScanToolStripMenuItem_Click);
            // 
            // mmToolStripMenuItem
            // 
            this.mmToolStripMenuItem.Name = "mmToolStripMenuItem";
            this.mmToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.mmToolStripMenuItem.Tag = "100";
            this.mmToolStripMenuItem.Text = "100mm";
            this.mmToolStripMenuItem.Click += new System.EventHandler(this.WaferScanToolStripMenuItem_Click);
            // 
            // mmToolStripMenuItem1
            // 
            this.mmToolStripMenuItem1.Name = "mmToolStripMenuItem1";
            this.mmToolStripMenuItem1.Size = new System.Drawing.Size(114, 22);
            this.mmToolStripMenuItem1.Tag = "150";
            this.mmToolStripMenuItem1.Text = "150mm";
            this.mmToolStripMenuItem1.Click += new System.EventHandler(this.WaferScanToolStripMenuItem_Click);
            // 
            // mmToolStripMenuItem2
            // 
            this.mmToolStripMenuItem2.Name = "mmToolStripMenuItem2";
            this.mmToolStripMenuItem2.Size = new System.Drawing.Size(114, 22);
            this.mmToolStripMenuItem2.Tag = "200";
            this.mmToolStripMenuItem2.Text = "200mm";
            this.mmToolStripMenuItem2.Click += new System.EventHandler(this.WaferScanToolStripMenuItem_Click);
            // 
            // shuttleToSensorToolStripMenuItem
            // 
            this.shuttleToSensorToolStripMenuItem.BackColor = System.Drawing.Color.Thistle;
            this.shuttleToSensorToolStripMenuItem.Name = "shuttleToSensorToolStripMenuItem";
            this.shuttleToSensorToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.shuttleToSensorToolStripMenuItem.Text = "Shuttle to sensor";
            this.shuttleToSensorToolStripMenuItem.Click += new System.EventHandler(this.ShuttleToSensorToolStripMenuItem_Click);
            // 
            // shuttleToCameraToolStripMenuItem
            // 
            this.shuttleToCameraToolStripMenuItem.BackColor = System.Drawing.Color.Thistle;
            this.shuttleToCameraToolStripMenuItem.Name = "shuttleToCameraToolStripMenuItem";
            this.shuttleToCameraToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.shuttleToCameraToolStripMenuItem.Text = "Shuttle to camera";
            this.shuttleToCameraToolStripMenuItem.Click += new System.EventHandler(this.shuttleToCameraToolStripMenuItem_Click);
            // 
            // scanParametersToolStripMenuItem
            // 
            this.scanParametersToolStripMenuItem.BackColor = System.Drawing.Color.Thistle;
            this.scanParametersToolStripMenuItem.Name = "scanParametersToolStripMenuItem";
            this.scanParametersToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.scanParametersToolStripMenuItem.Text = "Scan parameters";
            this.scanParametersToolStripMenuItem.Click += new System.EventHandler(this.scanParametersToolStripMenuItem_Click);
            // 
            // exportPlotDataToolStripMenuItem
            // 
            this.exportPlotDataToolStripMenuItem.BackColor = System.Drawing.Color.PaleGreen;
            this.exportPlotDataToolStripMenuItem.Name = "exportPlotDataToolStripMenuItem";
            this.exportPlotDataToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.exportPlotDataToolStripMenuItem.Text = "Export plot data";
            this.exportPlotDataToolStripMenuItem.Click += new System.EventHandler(this.ExportPlotDataToolStripMenuItem_Click);
            // 
            // calibrateSidecamToolStripMenuItem
            // 
            this.calibrateSidecamToolStripMenuItem.BackColor = System.Drawing.SystemColors.Info;
            this.calibrateSidecamToolStripMenuItem.Name = "calibrateSidecamToolStripMenuItem";
            this.calibrateSidecamToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.calibrateSidecamToolStripMenuItem.Text = "Calibrate sidecam";
            this.calibrateSidecamToolStripMenuItem.Click += new System.EventHandler(this.calibrateSidecamToolStripMenuItem_Click);
            // 
            // archiveLogsToolStripMenuItem
            // 
            this.archiveLogsToolStripMenuItem.BackColor = System.Drawing.SystemColors.Info;
            this.archiveLogsToolStripMenuItem.Name = "archiveLogsToolStripMenuItem";
            this.archiveLogsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.archiveLogsToolStripMenuItem.Text = "Archive logs";
            this.archiveLogsToolStripMenuItem.Click += new System.EventHandler(this.archiveLogsToolStripMenuItem_Click);
            // 
            // resetPlotToolStripMenuItem
            // 
            this.resetPlotToolStripMenuItem.BackColor = System.Drawing.Color.LightCoral;
            this.resetPlotToolStripMenuItem.Name = "resetPlotToolStripMenuItem";
            this.resetPlotToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.resetPlotToolStripMenuItem.Text = "Reset plot";
            this.resetPlotToolStripMenuItem.Click += new System.EventHandler(this.ResetPlotToolStripMenuItem_Click);
            // 
            // SEYRtoolStripButton
            // 
            this.SEYRtoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SEYRtoolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SEYRtoolStripButton.Image")));
            this.SEYRtoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SEYRtoolStripButton.Name = "SEYRtoolStripButton";
            this.SEYRtoolStripButton.ShowDropDownArrow = false;
            this.SEYRtoolStripButton.Size = new System.Drawing.Size(20, 22);
            this.SEYRtoolStripButton.Text = "SEYR Functions";
            this.SEYRtoolStripButton.Click += new System.EventHandler(this.SEYRtoolStripButton_Click);
            // 
            // toolStripButtonRegistration
            // 
            this.toolStripButtonRegistration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRegistration.Image = global::SPOT.Properties.Resources.iconmonstr_clipboard_6_16;
            this.toolStripButtonRegistration.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRegistration.Name = "toolStripButtonRegistration";
            this.toolStripButtonRegistration.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRegistration.Text = "Registration";
            this.toolStripButtonRegistration.Click += new System.EventHandler(this.ToolStripButtonRegistration_Click);
            // 
            // toolStripButtonOpenCompass
            // 
            this.toolStripButtonOpenCompass.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpenCompass.Image = global::SPOT.Properties.Resources.iconmonstr_compass_7_16;
            this.toolStripButtonOpenCompass.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpenCompass.Name = "toolStripButtonOpenCompass";
            this.toolStripButtonOpenCompass.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonOpenCompass.Text = "Open Compass";
            this.toolStripButtonOpenCompass.Click += new System.EventHandler(this.ToolStripButtonOpenCompass_Click);
            // 
            // toolStripButtonReset
            // 
            this.toolStripButtonReset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonReset.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonReset.Image")));
            this.toolStripButtonReset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReset.Name = "toolStripButtonReset";
            this.toolStripButtonReset.Size = new System.Drawing.Size(39, 22);
            this.toolStripButtonReset.Text = "Reset";
            this.toolStripButtonReset.Click += new System.EventHandler(this.ToolStripButtonReset_Click);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(36, 22);
            this.toolStripButtonHelp.Text = "Help";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(400, 22);
            this.toolStripProgressBar.Step = 1;
            this.toolStripProgressBar.Visible = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 486);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.tlpMain);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1110, 525);
            this.Name = "FormMain";
            this.Text = "SPOT";
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumZoom)).EndInit();
            this.tlbPosition.ResumeLayout(false);
            this.tlbPosition.PerformLayout();
            this.flowMotionHotkeys.ResumeLayout(false);
            this.flowMotionHotkeys.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumWhiteLED)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumRedLED)).EndInit();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlbPosition;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Label lblZ;
        public System.Windows.Forms.Button btnHomeAllStages;
        public System.Windows.Forms.Label lblXpos;
        public System.Windows.Forms.Label lblYpos;
        public System.Windows.Forms.Label lblZpos;
        private System.Windows.Forms.FlowLayoutPanel flowMotionHotkeys;
        private System.Windows.Forms.Button btnToCenter;
        public System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.Label lblH;
        public System.Windows.Forms.PictureBox pbxVision;
        public System.Windows.Forms.ToolStripButton toolStripButtonOpenCompass;
        private System.Windows.Forms.ToolStrip toolStrip;
        public System.Windows.Forms.ToolStripButton toolStripButtonReset;
        public System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        public System.Windows.Forms.ToolStripButton toolStripButtonRegistration;
        private System.Windows.Forms.ToolStripDropDownButton toolsStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem resetPlotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportPlotDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shuttleToSensorToolStripMenuItem;
        private System.Windows.Forms.Button btnToLoad;
        private System.Windows.Forms.ToolStripMenuItem waferScanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mmToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mmToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mmToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mmToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem mmToolStripMenuItem5;
        public OxyPlot.WindowsForms.PlotView plotView;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lblIntensity;
        private System.Windows.Forms.ToolStripButton toolStripButtonSwitchFeed;
        private System.Windows.Forms.ToolStripMenuItem stampScanToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.ToolStripMenuItem scanParametersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archiveLogsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calibrateSidecamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shuttleToCameraToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton SEYRtoolStripButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown NumZoom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowZoom;
        public System.Windows.Forms.NumericUpDown NumWhiteLED;
        public System.Windows.Forms.NumericUpDown NumRedLED;
    }
}