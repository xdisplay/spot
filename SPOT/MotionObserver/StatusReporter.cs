﻿using System;
using System.Windows.Forms;
using static SPOT.Functions.Global;

namespace SPOT
{
    public class StatusReporter : IObserver<Status>
    {
        public static Status Status;

        private IDisposable unsubscriber;
        private string instName;

        public StatusReporter(string name)
        {
            instName = name;
        }

        public string Name
        { get { return this.instName; } }

        public virtual void Subscribe(IObservable<Status> provider)
        {
            if (provider != null)
                unsubscriber = provider.Subscribe(this);
        }

        public void OnCompleted()
        {
            Console.WriteLine("The Location Tracker has completed transmitting data to {0}.", this.Name);
            Unsubscribe();
        }

        public void OnError(Exception error)
        {
            Console.WriteLine("{0}: The location cannot be determined.", this.Name);
        }

        public void OnNext(Status value)
        {
            Status = value;

            // Check if window is available
            if (Components.FormMain.IsHandleCreated)
            {
                // Update position labels
                Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.lblXpos.Text = Math.Round(value.X, 3).ToString(); });
                Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.lblYpos.Text = Math.Round(value.Y, 3).ToString(); });
                Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.lblZpos.Text = Math.Round(value.Z, 3).ToString(); });

                // Format Height Sensor reading
                string output = "0.000";
                if (value.H > 0 && value.I > 0)
                    output = Math.Round(value.H, 3).ToString();

                // Update Height Sensor labels
                Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.lblHeight.Text = output; });
                Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.lblIntensity.Text = Math.Round(value.I).ToString(); });

                // Color the homing button accordingly
                if (value.Homed)
                    Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.btnHomeAllStages.FlatAppearance.BorderColor = System.Drawing.Color.Green; });
                else
                    Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.btnHomeAllStages.FlatAppearance.BorderColor = System.Drawing.Color.Red; });

                // Color the reset button accordingly
                if (value.Fault)
                {
                    Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.toolStripButtonReset.BackColor = System.Drawing.Color.Red; });
                    CTS.Cancel();
                }
            }

            // Plot XYH values if the plot is ready
            if (Utility.Plotter.Initialized && value.I > 0)
                Utility.Plotter.Plot(value.X, value.Y, value.Z, value.H, value.I);
        }

        public virtual void Unsubscribe()
        {
            unsubscriber.Dispose();
        }
    }
}
