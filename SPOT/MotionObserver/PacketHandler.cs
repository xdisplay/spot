﻿using Aerotech.A3200.Status;

namespace SPOT
{
    static class PacketHandler
    {
        internal static void NewDiagPacketArrived(object sender, NewDiagPacketArrivedEventArgs e)
        {
            // Get Height Sensor data
            int[] rawData = { 0, 0 }; // Intensity, Dist
            double[] scaledData = { 0, 0 };
            Components.HeightSensor.Poll(rawData, scaledData, 2);

            // Check that all stages are homed
            bool homed = e.Data[0].AxisStatus.Homed && e.Data[1].AxisStatus.Homed && e.Data[2].AxisStatus.Homed;

            // Check for active faults
            bool fault = !e.Data[0].AxisFault.None && !e.Data[1].AxisFault.None && !e.Data[2].AxisFault.None;

            // Send info to provider
            Components.Provider.TrackStatus(new Status(e.Data[0].PositionFeedback,
                e.Data[1].PositionFeedback, e.Data[2].PositionFeedback, scaledData[1], scaledData[0], homed, fault));
        }
    }
}
