﻿using System;
using System.Collections.Generic;

namespace SPOT
{
    public class StatusTracker : IObservable<Status>
    {
        public StatusTracker()
        {
            observers = new List<IObserver<Status>>();
        }

        private List<IObserver<Status>> observers;

        public IDisposable Subscribe(IObserver<Status> observer)
        {
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new Unsubscriber(observers, observer);
        }

        private class Unsubscriber : IDisposable
        {
            private List<IObserver<Status>> _observers;
            private IObserver<Status> _observer;

            public Unsubscriber(List<IObserver<Status>> observers, IObserver<Status> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        public void TrackStatus(Status? loc)
        {
            foreach (var observer in observers)
            {
                if (!loc.HasValue)
                    observer.OnError(new LocationUnknownException());
                else
                    observer.OnNext(loc.Value);
            }
        }

        public void EndTransmission()
        {
            foreach (var observer in observers.ToArray())
                if (observers.Contains(observer))
                    observer.OnCompleted();

            observers.Clear();
        }
    }

    public class LocationUnknownException : Exception
    {
        internal LocationUnknownException()
        { }
    }
}
