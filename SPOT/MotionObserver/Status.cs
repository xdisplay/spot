﻿namespace SPOT
{
    public struct Status
    {
        double _X, _Y, _Z, _H, _I;
        bool _Homed;
        bool _Fault;

        public Status(double _X, double _Y, double _Z, double _H, double _I, bool _Homed, bool _Fault)
        {
            this._X = _X;
            this._Y = _Y;
            this._Z = _Z;
            this._H = _H;
            this._I = _I <= 100.0 ? _I : 0.0;
            this._Homed = _Homed;
            this._Fault = _Fault;
        }

        public double X
        { get { return this._X; } }

        public double Y
        { get { return this._Y; } }

        public double Z
        { get { return this._Z; } }

        public double H
        { get { return this._H; } }

        public double I
        { get { return this._I; } }

        public bool Homed
        { get { return this._Homed; } }

        public bool Fault
        { get { return this._Fault; } }
    }
}
