﻿
namespace SPOT
{
    partial class Compass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpCompass = new System.Windows.Forms.TableLayoutPanel();
            this.btnNE = new System.Windows.Forms.Button();
            this.btnNW = new System.Windows.Forms.Button();
            this.btnN = new System.Windows.Forms.Button();
            this.btnW = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.btnE = new System.Windows.Forms.Button();
            this.btnSE = new System.Windows.Forms.Button();
            this.btnS = new System.Windows.Forms.Button();
            this.btnSW = new System.Windows.Forms.Button();
            this.numRow = new System.Windows.Forms.NumericUpDown();
            this.numCol = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGoToIndex = new System.Windows.Forms.Button();
            this.cbxAtSensor = new System.Windows.Forms.CheckBox();
            this.tlpCompass.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCol)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpCompass
            // 
            this.tlpCompass.ColumnCount = 6;
            this.tlpCompass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tlpCompass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpCompass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpCompass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpCompass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tlpCompass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCompass.Controls.Add(this.btnNE, 1, 1);
            this.tlpCompass.Controls.Add(this.btnNW, 3, 1);
            this.tlpCompass.Controls.Add(this.btnN, 2, 1);
            this.tlpCompass.Controls.Add(this.btnW, 1, 2);
            this.tlpCompass.Controls.Add(this.btnC, 2, 2);
            this.tlpCompass.Controls.Add(this.btnE, 3, 2);
            this.tlpCompass.Controls.Add(this.btnSE, 1, 3);
            this.tlpCompass.Controls.Add(this.btnS, 2, 3);
            this.tlpCompass.Controls.Add(this.btnSW, 3, 3);
            this.tlpCompass.Controls.Add(this.numRow, 1, 6);
            this.tlpCompass.Controls.Add(this.numCol, 2, 6);
            this.tlpCompass.Controls.Add(this.label1, 1, 5);
            this.tlpCompass.Controls.Add(this.label2, 2, 5);
            this.tlpCompass.Controls.Add(this.btnGoToIndex, 3, 6);
            this.tlpCompass.Controls.Add(this.cbxAtSensor, 1, 8);
            this.tlpCompass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCompass.Location = new System.Drawing.Point(0, 0);
            this.tlpCompass.Name = "tlpCompass";
            this.tlpCompass.RowCount = 10;
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpCompass.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCompass.Size = new System.Drawing.Size(157, 247);
            this.tlpCompass.TabIndex = 0;
            // 
            // btnNE
            // 
            this.btnNE.BackColor = System.Drawing.Color.AliceBlue;
            this.btnNE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNE.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnNE.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnNE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNE.Location = new System.Drawing.Point(13, 13);
            this.btnNE.Name = "btnNE";
            this.btnNE.Size = new System.Drawing.Size(40, 40);
            this.btnNE.TabIndex = 1;
            this.btnNE.Text = "NW";
            this.btnNE.UseVisualStyleBackColor = false;
            this.btnNE.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // btnNW
            // 
            this.btnNW.BackColor = System.Drawing.Color.AliceBlue;
            this.btnNW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNW.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnNW.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnNW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNW.Location = new System.Drawing.Point(105, 13);
            this.btnNW.Name = "btnNW";
            this.btnNW.Size = new System.Drawing.Size(40, 40);
            this.btnNW.TabIndex = 2;
            this.btnNW.Text = "NE";
            this.btnNW.UseVisualStyleBackColor = false;
            this.btnNW.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // btnN
            // 
            this.btnN.BackColor = System.Drawing.Color.AliceBlue;
            this.btnN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnN.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnN.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnN.Location = new System.Drawing.Point(59, 13);
            this.btnN.Name = "btnN";
            this.btnN.Size = new System.Drawing.Size(40, 40);
            this.btnN.TabIndex = 0;
            this.btnN.Text = "N";
            this.btnN.UseVisualStyleBackColor = false;
            this.btnN.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // btnW
            // 
            this.btnW.BackColor = System.Drawing.Color.AliceBlue;
            this.btnW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnW.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnW.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnW.Location = new System.Drawing.Point(13, 59);
            this.btnW.Name = "btnW";
            this.btnW.Size = new System.Drawing.Size(40, 40);
            this.btnW.TabIndex = 3;
            this.btnW.Text = "W";
            this.btnW.UseVisualStyleBackColor = false;
            this.btnW.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // btnC
            // 
            this.btnC.BackColor = System.Drawing.Color.AliceBlue;
            this.btnC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnC.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnC.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnC.Location = new System.Drawing.Point(59, 59);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(40, 40);
            this.btnC.TabIndex = 4;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = false;
            this.btnC.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // btnE
            // 
            this.btnE.BackColor = System.Drawing.Color.AliceBlue;
            this.btnE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnE.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnE.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnE.Location = new System.Drawing.Point(105, 59);
            this.btnE.Name = "btnE";
            this.btnE.Size = new System.Drawing.Size(40, 40);
            this.btnE.TabIndex = 5;
            this.btnE.Text = "E";
            this.btnE.UseVisualStyleBackColor = false;
            this.btnE.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // btnSE
            // 
            this.btnSE.BackColor = System.Drawing.Color.AliceBlue;
            this.btnSE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSE.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnSE.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnSE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSE.Location = new System.Drawing.Point(13, 105);
            this.btnSE.Name = "btnSE";
            this.btnSE.Size = new System.Drawing.Size(40, 40);
            this.btnSE.TabIndex = 6;
            this.btnSE.Text = "SW";
            this.btnSE.UseVisualStyleBackColor = false;
            this.btnSE.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // btnS
            // 
            this.btnS.BackColor = System.Drawing.Color.AliceBlue;
            this.btnS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnS.Location = new System.Drawing.Point(59, 105);
            this.btnS.Name = "btnS";
            this.btnS.Size = new System.Drawing.Size(40, 40);
            this.btnS.TabIndex = 7;
            this.btnS.Text = "S";
            this.btnS.UseVisualStyleBackColor = false;
            this.btnS.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // btnSW
            // 
            this.btnSW.BackColor = System.Drawing.Color.AliceBlue;
            this.btnSW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSW.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CadetBlue;
            this.btnSW.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue;
            this.btnSW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSW.Location = new System.Drawing.Point(105, 105);
            this.btnSW.Name = "btnSW";
            this.btnSW.Size = new System.Drawing.Size(40, 40);
            this.btnSW.TabIndex = 8;
            this.btnSW.Text = "SE";
            this.btnSW.UseVisualStyleBackColor = false;
            this.btnSW.Click += new System.EventHandler(this.BtnPost_Click);
            // 
            // numRow
            // 
            this.numRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numRow.Location = new System.Drawing.Point(13, 175);
            this.numRow.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numRow.Name = "numRow";
            this.numRow.Size = new System.Drawing.Size(40, 20);
            this.numRow.TabIndex = 10;
            this.numRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numCol
            // 
            this.numCol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numCol.Location = new System.Drawing.Point(59, 175);
            this.numCol.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numCol.Name = "numCol";
            this.numCol.Size = new System.Drawing.Size(40, 20);
            this.numCol.TabIndex = 11;
            this.numCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 12);
            this.label1.TabIndex = 12;
            this.label1.Text = "Row";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(59, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "Column";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnGoToIndex
            // 
            this.btnGoToIndex.AutoSize = true;
            this.btnGoToIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGoToIndex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGoToIndex.Location = new System.Drawing.Point(105, 173);
            this.btnGoToIndex.Name = "btnGoToIndex";
            this.btnGoToIndex.Size = new System.Drawing.Size(40, 25);
            this.btnGoToIndex.TabIndex = 14;
            this.btnGoToIndex.Text = "Go";
            this.btnGoToIndex.UseVisualStyleBackColor = true;
            this.btnGoToIndex.Click += new System.EventHandler(this.BtnGoToIndex_Click);
            // 
            // cbxAtSensor
            // 
            this.cbxAtSensor.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxAtSensor.AutoSize = true;
            this.cbxAtSensor.BackColor = System.Drawing.Color.Khaki;
            this.tlpCompass.SetColumnSpan(this.cbxAtSensor, 3);
            this.cbxAtSensor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxAtSensor.FlatAppearance.CheckedBackColor = System.Drawing.Color.LawnGreen;
            this.cbxAtSensor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxAtSensor.Location = new System.Drawing.Point(13, 214);
            this.cbxAtSensor.Name = "cbxAtSensor";
            this.cbxAtSensor.Size = new System.Drawing.Size(132, 23);
            this.cbxAtSensor.TabIndex = 15;
            this.cbxAtSensor.Text = "At Sensor";
            this.cbxAtSensor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbxAtSensor.UseVisualStyleBackColor = false;
            this.cbxAtSensor.CheckedChanged += new System.EventHandler(this.cbxAtSensor_CheckedChanged);
            // 
            // Compass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(157, 247);
            this.Controls.Add(this.tlpCompass);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Compass";
            this.Text = "Compass";
            this.tlpCompass.ResumeLayout(false);
            this.tlpCompass.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCol)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpCompass;
        private System.Windows.Forms.Button btnNE;
        private System.Windows.Forms.Button btnNW;
        private System.Windows.Forms.Button btnN;
        private System.Windows.Forms.Button btnW;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnE;
        private System.Windows.Forms.Button btnSE;
        private System.Windows.Forms.Button btnS;
        private System.Windows.Forms.Button btnSW;
        private System.Windows.Forms.NumericUpDown numRow;
        private System.Windows.Forms.NumericUpDown numCol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGoToIndex;
        public System.Windows.Forms.CheckBox cbxAtSensor;
    }
}