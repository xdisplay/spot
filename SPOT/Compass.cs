﻿using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static SPOT.Functions.Global;
using static SPOT.Functions.Stages;

namespace SPOT
{
    public partial class Compass : Form
    {
        public static string StampRegistrationPath = @"C:\SPOT\StampRegistration.txt";
        public static PointF SWpost = new PointF(0, 0);
        public static PointF SEpost = new PointF(0, 0);

        // Defaults from SN160145
        public static PointF PostPitch = new PointF(0.2f, 0.2f);
        public static PointF NumPosts = new PointF(128, 128);

        public static bool MovingToPost;
        public static bool AtSensor;

        private static PointF CenterOfRotation;

        public Compass()
        {
            InitializeComponent();
            Show();
        }

        ///<Summary>
        /// Send a compass position and move the optics there, with optional alignment correction
        ///</Summary>
        public async static Task ToStampPost(string ID, float i = 0, float j = 0, bool scan = false, CancellationToken token = default, double speed = 15.00)
        {
            while (!token.IsCancellationRequested)
            {
                if (!scan)
                    scan = AtSensor;

                PointF PostPos;

                if (MovingToPost)
                    return;
                else
                    MovingToPost = true;

                if (ID == "Index")
                {
                    if (i > NumPosts.X | j > NumPosts.Y)
                    {
                        MovingToPost = false;
                        return;
                    }

                    PostPos = GetPostPos(i, j);
                }
                else
                    PostPos = ParseStampCompass(ID);

                RotatePoint(ref PostPos);

                if (scan)
                {
                    PostPos.X += (float)ComponentHeightSensor.HeightSensePos.X;
                    PostPos.Y += (float)ComponentHeightSensor.HeightSensePos.Y;
                }

                await Task.Run(() => MovePair(PostPos.X, PostPos.Y, speed, AbsMove, 5000, CTS.Token));
                Thread.SpinWait(100);
                MovingToPost = false;
                return;
            }
        }

        ///<Summary>
        /// Send a compass position and get the optics coordinates of the post
        ///</Summary>
        public static PointF ParseStampCompass(string Corner)
        {
            int i = 0;
            int j = 0;

            switch (Corner)
            {
                case "NW":
                    {
                        i = 0;
                        j = (int)(NumPosts.Y - 1);
                        break;
                    }

                case "NE":
                    {
                        i = (int)(NumPosts.X - 1);
                        j = (int)(NumPosts.Y - 1);
                        break;
                    }

                case "SE":
                    {
                        i = (int)(NumPosts.X - 1);
                        j = 0;
                        break;
                    }

                case "SW":
                    {
                        i = 0;
                        j = 0;
                        break;
                    }

                case "N":
                    {
                        i = (int)Math.Floor((NumPosts.X - 1) / 2);
                        j = (int)(NumPosts.Y - 1);
                        break;
                    }

                case "S":
                    {
                        i = (int)Math.Floor((NumPosts.X - 1) / 2);
                        j = 0;
                        break;
                    }

                case "W":
                    {
                        j = (int)Math.Floor((NumPosts.Y - 1) / 2);
                        i = 0;
                        break;
                    }

                case "E":
                    {
                        j = (int)Math.Floor((NumPosts.Y - 1) / 2);
                        i = (int)(NumPosts.X - 1);
                        break;
                    }

                case "C":
                    {
                        i = (int)Math.Floor((NumPosts.X - 1) / 2);
                        j = (int)Math.Floor((NumPosts.Y - 1) / 2);
                        break;
                    }
            }

            return GetPostPos(i, j);
        }

        public static void RotatePoint(ref PointF point, bool getCOR = false)
        {
            if (point == SWpost && !getCOR)
                return;
            double angle = -1 * Math.Atan((SEpost.Y - SWpost.Y) / Math.Abs(SEpost.X - SWpost.X));
            float x = (float)(point.X * Math.Cos(angle) - point.Y * Math.Sin(angle));
            float y = (float)(point.X * Math.Sin(angle) + point.Y * Math.Cos(angle));
            point = new PointF(x + CenterOfRotation.X, y + CenterOfRotation.Y);
        }

        public static void UpdateCenterOfRotation()
        {
            CenterOfRotation = new PointF(0, 0);
            PointF SWpostCopy = SWpost;
            RotatePoint(ref SWpostCopy, true);
            CenterOfRotation = new PointF(SWpost.X - SWpostCopy.X, SWpost.Y - SWpostCopy.Y);
        }

        public static PointF GetPostPos(float i, float j)
        {
            PointF point = new PointF(0, 0);

            if (i < NumPosts.X & j < NumPosts.Y)
            {
                point.X = SWpost.X - i * PostPitch.X;
                point.Y = SWpost.Y - j * PostPitch.Y;
            }

            return point;
        }

        private async void BtnPost_Click(object sender, EventArgs e)
        {
            if (Application.UseWaitCursor) return;
            using (var wc = new Utility.WaitCursor())
                await Task.Run(() => ToStampPost(((Button)sender).Text, scan: AtSensor, token: CTS.Token));
        }

        private async void BtnGoToIndex_Click(object sender, EventArgs e)
        {
            bool forceMax = false;
            if (numRow.Value > (decimal)NumPosts.Y)
            {
                numRow.Value = (decimal)NumPosts.Y;
                forceMax = true;
            }
            if (numRow.Value > (decimal)NumPosts.X)
            {
                numRow.Value = (decimal)NumPosts.X;
                forceMax = true;
            }
            if (forceMax)
                return;

            if (Application.UseWaitCursor) return;
            using (var wc = new Utility.WaitCursor())
                await Task.Run(() => ToStampPost("Index", (int)numCol.Value, (int)numRow.Value, scan: AtSensor, token: CTS.Token));
        }

        private void cbxAtSensor_CheckedChanged(object sender, EventArgs e)
        {
            AtSensor = cbxAtSensor.Checked;
            if (!cbxAtSensor.Checked)
                cbxAtSensor.BackColor = SystemColors.Control;
        }
    }
}
