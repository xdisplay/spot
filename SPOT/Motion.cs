﻿using Aerotech.A3200;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using static SPOT.Components;
using static SPOT.Functions.Global;
using static SPOT.Functions.Stages;

namespace SPOT
{
    public partial class Motion
    {
        public enum GUISpeed
        {
            Slow,
            Medium,
            Fast
        }

        public GUISpeed Speed = GUISpeed.Slow;

        private bool _FreeRun = false;

        public Motion()
        {
            InitializeComponent();
        }

        //                                            N            NE           E             SE            S              SW            W             NW           UP         DOWN
        private int[,] ButtonAxes = new int[,] { { 0, 1, 0 }, { 1, 1, 0 }, { 1, 0, 0 }, { 1, -1, 0 }, { 0, -1, 0 }, { -1, -1, 0 }, { -1, 0, 0 }, { -1, 1, 0 }, { 0, 0, 1 }, { 0, 0, -1 } };

        private void XYMotion_Load(object sender, EventArgs e)
        {
            foreach (Button btn in tlpButtons.Controls.OfType<Button>())
            {
                if (btn.Tag.ToString() != "10")
                {
                    btn.MouseDown += Btn_MouseDown;
                    btn.MouseUp += Btn_MouseUp;
                }
            }
            btnSlow.PerformClick();
            ToggleFreeRun(true);
        }

        private void Btn_MouseDown(object sender, MouseEventArgs e)
        {
            int tag = int.Parse(((Button)sender).Tag.ToString());
            int XDir = ButtonAxes[tag, 0];
            int YDir = ButtonAxes[tag, 1];
            int ZDir = ButtonAxes[tag, 2];

            try
            {
                if (_FreeRun == true)
                {
                    if (XDir != 0)
                        MotionController.FreeRun(XAxis, XDir * XYSpeed[(int)Speed]);
                    if (YDir != 0)
                        MotionController.FreeRun(YAxis, YDir * XYSpeed[(int)Speed]);
                    if (ZDir != 0)
                        MotionController.FreeRun(ZAxis, ZDir * ZSpeed[(int)Speed]);
                }
                else
                {
                    if (XDir != 0)
                        MotionController.MoveInc(XAxis, (double)(XDir * numXincrement.Value / 1000), XYSpeed[(int)Speed]);
                    if (YDir != 0)
                        MotionController.MoveInc(YAxis, (double)(YDir * numYincrement.Value / 1000), XYSpeed[(int)Speed]);
                    if (ZDir != 0)
                        MotionController.MoveInc(ZAxis, (double)(ZDir * numZincrement.Value / 1000), ZSpeed[(int)Speed]);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message); // Commanded into fault position
            }

            Refresh();
        }

        private void Btn_MouseUp(object sender, MouseEventArgs e)
        {
            if (_FreeRun)
            {
                int tag = int.Parse(((Button)sender).Tag.ToString());
                if (ButtonAxes[tag, 0] != 0)
                    MotionController.FreeRunStop(XAxis);
                if (ButtonAxes[tag, 1] != 0)
                    MotionController.FreeRunStop(YAxis);
                if (ButtonAxes[tag, 2] != 0)
                    MotionController.FreeRunStop(ZAxis);
            }

            Refresh();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            CTS.Cancel();
            if (!DUMMY) MotionController.AbortMotion(AxisMask.All);
            Components.FormMain.toolStripButtonReset.BackColor = Color.Red;
            Components.FormMain.toolStripProgressBar.Visible = false;
            Refresh();
        }

        private void btnSpeed_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Speed = (GUISpeed)int.Parse(button.Tag.ToString());
            ColorSpeedButton(button);
        }

        private void ColorSpeedButton(Button button)
        {
            // Clear buttons
            btnSlow.BackColor = Color.Transparent;
            btnMedium.BackColor = Color.Transparent;
            btnFast.BackColor = Color.Transparent;
            btnSlow.ForeColor = Color.Black;
            btnMedium.ForeColor = Color.Black;
            btnFast.ForeColor = Color.Black;

            // Set color
            button.BackColor = Color.CornflowerBlue;
            button.ForeColor = Color.White;

            Refresh();
        }

        private void cbxFreeRun_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            ToggleFreeRun(checkBox.Checked);
        }

        private void ToggleFreeRun(bool freeRunEnabled)
        {
            _FreeRun = freeRunEnabled;
            if (_FreeRun)
            {
                numXincrement.BackColor = Color.Gainsboro;
                numYincrement.BackColor = Color.Gainsboro;
                numZincrement.BackColor = Color.Gainsboro;
            }
            else
            {
                numXincrement.BackColor = Color.White;
                numYincrement.BackColor = Color.White;
                numZincrement.BackColor = Color.White;
            }

            Refresh();
        }

        private async void btnToZero_Click(object sender, EventArgs e)
        {
            if (Application.UseWaitCursor) return;
            using (var wc = new SPOT.Utility.WaitCursor())
                await Task.Run(() => SPOT.Functions.Stages.Move(ZAxis, 0, XYSpeed[2], AbsMove, 5000, CTS.Token));
        }
    }
}