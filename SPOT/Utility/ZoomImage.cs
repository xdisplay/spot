﻿using System.Drawing;

namespace SPOT.Utility
{
    public static class ZoomImage
    {
        private static double _Percentage = 0.5;
        public static double Percentage 
        {
            get => _Percentage;
            set
            {
                _Percentage = 1 - (value / 100);
                ForceUpdate = true;
            }
        }
        private static bool ForceUpdate = false;
        private static RectangleF ZoomArea = RectangleF.Empty;

        public static Bitmap NewFrame(Bitmap bitmap)
        {
            if (ZoomArea == RectangleF.Empty || ForceUpdate)
            {
                RectangleF rect = new RectangleF(
                    (float)((bitmap.Width / 2.0) - (bitmap.Width * _Percentage / 2.0)),
                    (float)((bitmap.Height / 2.0) - (bitmap.Height * _Percentage / 2.0)),
                    (float)(bitmap.Width * _Percentage),
                    (float)(bitmap.Height * _Percentage));
                if (rect.Width != 0 && rect.Height != 0) ZoomArea = rect;
                ForceUpdate = false;
            }
            return bitmap.Clone(ZoomArea, bitmap.PixelFormat);
        }
    }
}
