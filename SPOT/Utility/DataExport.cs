﻿using System;
using System.Windows.Forms;

namespace SPOT.Utility
{
    public partial class DataExport : Form
    {
        public string ExportName { get; set; }

        public DataExport()
        {
            InitializeComponent();
            txtName.KeyDown += TxtName_KeyDown;
        }

        private void TxtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Proceed();
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            Proceed();
        }

        private void btnDiscard_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnCustomLocation_Click(object sender, EventArgs e)
        {
            ExportName = txtName.Text;
            DialogResult = DialogResult.Ignore;
        }

        private void Proceed()
        {
            ExportName = txtName.Text;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
