﻿using System.ComponentModel;
using System.Windows.Forms;

namespace SPOT.Utility
{
    public partial class ScanParameters : Form
    {
        private readonly double MaxSpeed = 20.0;
        private readonly double MinSpeed = 0.001;
        private double _ScanSpeed = 15.0;
        [
            Category("User Parameters"), 
            DisplayName("Scan Speed"),
            Description("Speed of scans in mm/s")
        ]
        public double ScanSpeed
        {
            get => _ScanSpeed;
            set
            {
                if (value > MaxSpeed)
                    _ScanSpeed = MaxSpeed;
                else if (value < MinSpeed)
                    _ScanSpeed = MinSpeed;
                else
                    _ScanSpeed = value;
            }
        }

        private int _NumPasses = 10;
        [
            Category("User Parameters"),
            DisplayName("Number of Passes"),
            Description("The approximate number of linear slices in the scan. Actual number will vary based on scan area divisibility.")
        ]
        public int NumPasses
        {
            get => _NumPasses;
            set
            {
                if (value < 1)
                    _NumPasses = 1;
                else
                    _NumPasses = value;
            }
        }

        public ScanParameters()
        {
            InitializeComponent();
            propertyGrid.BrowsableAttributes = new AttributeCollection(new CategoryAttribute("User Parameters"));
            propertyGrid.SelectedObject = this;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }
    }
}
