﻿using System.Runtime.InteropServices;

namespace SPOT.Utility
{
    static class THUM
    {
        [DllImport(@"Utility/thum.dll")]
        public static extern double Read();

        [DllImport(@"Utility/thum.dll")]
        public static extern double GetTemp();

        [DllImport(@"Utility/thum.dll")]
        public static extern double GetRH();

        [DllImport(@"Utility/thum.dll")]
        public static extern int SetTempUnit(double val);

    }
}
