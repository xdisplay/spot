﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Legends;
using System;
using System.IO;
using System.Windows.Forms;
using static SPOT.Functions.Global;

namespace SPOT.Utility
{
    static class Plotter
    {
        public static PlotModel PlotModel = new PlotModel();
        public static LinearColorAxis HAxis;
        public static bool Initialized = false;
        public static bool Collect = true;
        public static double Baseline = 0.0;

        private static double MinZ = double.MaxValue;
        private static double MaxZ = 0;

        private static Legend Legend;
        private static ScatterSeries ScatterSeries = new ScatterSeries();
        private static string LogPath = @"C:\SPOT\Data\Log\HeightSensorLog.txt";
        private static string ServerLogPath = @"\\XDISPLAY-FS01.x-display.local\Company\Systems Eng\SPOT Network Folder\HeightSensorLog.txt";

        public static void InitializePlot(bool collectByDefault = true)
        {
            MinZ = double.MaxValue;
            MaxZ = 0;

            Collect = false;
            PlotModel = new PlotModel();

            LinearAxis XAxis = new LinearAxis()
            {
                Position = AxisPosition.Bottom,
                Minimum = 0,
                Maximum = 200,
                StartPosition = 1,
                EndPosition = 0,
                Title = "X Position (mm)"
            };

            LinearAxis YAxis = new LinearAxis()
            {
                Position = AxisPosition.Left,
                Minimum = 0,
                Maximum = 200,
                StartPosition = 1,
                EndPosition = 0,
                Title = "Y Position (mm)"
            };

            HAxis = new LinearColorAxis
            {
                Position = AxisPosition.Top,
                Key = "ColorAxis",
                StartPosition = 1,
                EndPosition = 0,
                Title = "Height Measurement (mm)"
            };

            Legend = new Legend()
            {
                LegendPlacement = LegendPlacement.Inside,
                LegendPosition = LegendPosition.RightTop,
                LegendBackground = OxyColors.Transparent,
                LegendBorder = OxyColors.Transparent,
                IsLegendVisible = true,
                ShowInvisibleSeries = false
            };

            ScatterSeries = new ScatterSeries();
            ScatterSeries.TrackerFormatString = "{Tag}";

            PlotModel.Axes.Add(XAxis);
            PlotModel.Axes.Add(YAxis);
            PlotModel.Axes.Add(HAxis);
            PlotModel.Legends.Add(Legend);

            Components.PlotView.Model = PlotModel;
            Components.PlotView.Refresh();

            Initialized = true;
            Collect = collectByDefault;
        }

        public static void Plot(double x, double y, double z, double h, double i)
        {
            if (!Components.FormMain.IsHandleCreated || !Components.PlotView.IsHandleCreated || !(h > 0)) return;

            try
            {
                lock(PlotModel.SyncRoot)
                {
                    if (Collect)
                    {
                        if (h < MinZ)
                        {
                            MinZ = h;
                            HAxis.Minimum = MinZ;
                        }
                        else if (h > MaxZ)
                        {
                            MaxZ = h;
                            HAxis.Maximum = MaxZ;
                        }
                        ScatterSeries.Points.Add(new ScatterPoint(x, y, 2, h, 
                            $"X: {x:#,0.000} mm\nY: {y:#,0.000} mm\nZ: {z:#,0.000} mm\nH: {h:#,0.000} mm\nI: {i:#,0} %"));
                        Legend.LegendTitle = $"Range = {(MaxZ - MinZ) * 1e3:#,0} μm";
                        if (ScatterSeries.Points.Count == 1) PlotModel.Series.Add(ScatterSeries); // We don't want a plot with 0 points, so wait until one exists
                    }
                }
                Components.PlotView.Invoke((MethodInvoker)delegate () { Components.PlotView.InvalidatePlot(true); });
            }
            catch (Exception) 
            {
                System.Diagnostics.Debug.WriteLine("Error in Plotter.Plot");
            }
        }

        public static void ExportData()
        {
            Collect = false;
            for (int i = 0; i < 10; i++) { System.Threading.Thread.Sleep(10); Application.DoEvents(); }

            string exportName = "SPOT";
            string exportPath = LogPath;
            using (DataExport dataExport = new DataExport())
            {
                var result = dataExport.ShowDialog();
                if (result == DialogResult.OK)
                    exportName = dataExport.ExportName;
                else if (result == DialogResult.Ignore)
                {
                    string customPath = SaveFile("Save SPOT Scan", "Text File (*.txt) | *.txt");
                    if (customPath == null) return;
                    exportName = dataExport.ExportName;
                    exportPath = customPath;
                }
                else
                    return;
            }

            if (!File.Exists(exportPath)) File.Create(exportPath);
            for (int i = 0; i < 100; i++) { System.Threading.Thread.Sleep(10); Application.DoEvents(); }
            if (IsFileLocked(exportPath))
            {
                MessageBox.Show("Could not export data to local file. Try closing windows explorer and XferSuite, then try to export again.");
                return;
            }

            FileStream stream = File.Open(exportPath, FileMode.Append, FileAccess.Write);
            THUM.Read();
            WriteToStream(stream, string.Format("{0}\tNEWSCAN\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\n",
                GetDT(), 
                exportName, 
                Math.Round(THUM.GetTemp(), 1), 
                Math.Round(THUM.GetRH()),
                Components.ScanParameters.ScanSpeed,
                Components.ScanParameters.NumPasses,
                ComponentHeightSensor.GetDetectionThreshold()));

            foreach (ScatterPoint point in ScatterSeries.Points)
                WriteToStream(stream, string.Format("{0}\t{1}\t{2}\t{3}\t{4}\n", GetDT(), point.X, point.Y, point.Value, ScatterTagToZI(point.Tag.ToString())));
            stream.Close();

            if (exportPath == LogPath)
            {
                if (File.Exists(ServerLogPath) && IsFileLocked(ServerLogPath))
                {
                    MessageBox.Show("Export to local copy successful. Could not sync file on network drive. Try closing windows explorer and XferSuite, then try to export again.");
                    return;
                }
                else
                {
                    try
                    {
                        File.Copy(LogPath, ServerLogPath, true);
                        MessageBox.Show("File synced with network drive and local copy.");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Export to local copy successful. Could not sync file on network drive. Try closing windows explorer and XferSuite, then try to export again.");
                    }
                }
            }
            else
            {
                MessageBox.Show("Scan exported to local copy.");
            }

            for (int i = 0; i < 10; i++) { System.Threading.Thread.Sleep(10); Application.DoEvents(); }
            Collect = true;
        }

        private static string ScatterTagToZI(string tag)
        {
            string[] cols = tag.Split('\n');
            double Z = double.Parse(cols[2].Split(' ')[1]);
            double I = double.Parse(cols[4].Split(' ')[1]);
            return $"{Z}\t{I}";
        }

        public static string SaveFile(string title, string filter)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.RestoreDirectory = true;
                saveFileDialog.Title = title;
                saveFileDialog.Filter = filter;
                saveFileDialog.OverwritePrompt = false;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    return saveFileDialog.FileName;
            }
            return null;
        }

        private static bool IsFileLocked(string path)
        {
            FileInfo file = new FileInfo(path);
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                return true;
            }
            return false;
        }
    }
}
