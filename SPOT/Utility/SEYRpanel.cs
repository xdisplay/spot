﻿using Aerotech.A3200.Status;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using static SPOT.Functions.Stages;
using static SPOT.Functions.Global;
using static SPOT.Components;

namespace SPOT.Utility
{
    public partial class SEYRpanel : Form
    {
        private double ImageSaveThreshold = -1;
        private readonly GridMaker.Composer GridComposer;
        private GridMaker.Generator Generator;
        private GridMaker.Generator.Node Origin;
        private bool ValidOrigin = false;
        private List<GridMaker.Generator.Node> Points = new List<GridMaker.Generator.Node>();

        /// <summary>
        /// Control panel for SEYR Metrology
        /// </summary>
        public SEYRpanel()
        {
            InitializeComponent();
            GridComposer = new GridMaker.Composer();
            FormClosing += SEYRpanel_FormClosing;
            NumImageThreshold.Value = (decimal)ImageSaveThreshold;
            CallbackToolStripComboBox.Items.AddRange(GetCallbackNames());
            CallbackToolStripComboBox.SelectedIndex = (int)CallbackHandler;
            CallbackToolStripComboBox.SelectedIndexChanged += CallbackToolStripComboBox_SelectedIndexChanged;
        }

        private void SEYRpanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void BtnOpen_Click(object sender, EventArgs e)
        {
            if (SEYRCh != null)
            {
                SEYRCh.OpenComposer(LastImage);
                return;
            }

            SEYR.Session.Channel channel = SEYR.Session.Channel.OpenSEYR();
            if (channel != null)
            {
                SEYRCh = channel;
                SEYRCh.SetPixelsPerMicron(1);
                PxPerMicronToolStripTextBox.Text = SEYRCh.PxPerMicron.ToString();
                RunToolStripMenuItem.Enabled = true;
                ToolsToolStripMenuItem.Enabled = true;
                BtnOpen.Text = "Open Composer";
            }
        }

        private async Task<bool> Run(bool stamp)
        {
            BeginInvoke((MethodInvoker)delegate ()
            {
                ProgressBar.Value = 0;
                ProgressBar.Maximum = Points.Count;
                TLP.Enabled = false;
            });

            int estimateIdx = 10;
            DateTime start = DateTime.Now;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < Points.Count; i++)
            {
                GridMaker.Generator.Node node = Points[i];

                if (i == estimateIdx)
                {
                    var time = sw.ElapsedMilliseconds;
                    sw.Stop();
                    DateTime end = start.AddMilliseconds(time * ((double)Points.Count / estimateIdx)).AddMinutes(5);
                    BeginInvoke((MethodInvoker)delegate () { LblCompleteTime.Text = $"Complete at {end:h:mm}"; });
                }

                PointF metroPos = Generator.GetStagePosition(node);
                if (float.IsNaN(metroPos.X) || float.IsNaN(metroPos.Y)) continue;
                await Task.Run(() => MovePair(metroPos.X, metroPos.Y, 15.00, AbsMove, 5000, CTS.Token));
                if (CTS.IsCancellationRequested) break;

                for (int j = 0; j < 5; j++) // Wait for image to steady
                {
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(10);
                }
                BeginInvoke((MethodInvoker)delegate ()
                {
                    ProgressBar.PerformStep();
                    NumAX.Value = node.A.Y + 1;
                    NumAY.Value = node.A.X + 1;
                    NumBX.Value = node.B.Y + 1;
                    NumBY.Value = node.B.X + 1;
                    NumCX.Value = node.C.Y + 1;
                    NumCY.Value = node.C.X + 1;
                    LblXPos.Text = $"X = {metroPos.X:f3}";
                    LblYPos.Text = $"Y = {metroPos.Y:f3}";
                });

                Bitmap image = LastImage;
                if (CTS.IsCancellationRequested) break;
                double info = await SEYRCh.NewImage(image, node.Callback && CallbackPF(), $"{i}\t{metroPos.X}\t{metroPos.Y}\t{node}", stamp);
                if ((ImageSaveThreshold != -1 && info > ImageSaveThreshold)
                    || (double.IsNaN(info) && ImageSaveThreshold == 0))
                    image.Save
                        ($"{SEYRCh.ImagesDirectory}" +
                         $"{metroPos.X}_{metroPos.Y}" +
                         $"_AX{node.A.X + 1}_AY{node.A.X + 1}" +
                         $"_BX{node.B.X + 1}_BY{node.B.X + 1}" +
                         $"_CX{node.C.X + 1}_CY{node.C.X + 1}.png");
                GC.Collect();
            }

            BeginInvoke((MethodInvoker)delegate () 
            { 
                ProgressBar.Value = 0;
                LblCompleteTime.Text = "Complete at N/A";
                TLP.Enabled = true;
            });
            SEYRCh.MakeArchive();
            return true;
        }

        private void BtnSetOrigin_Click(object sender, EventArgs e)
        {
            if (!ValidOrigin) // Reset to defaults
            {
                BtnGotoOrigin.Text = "Goto Origin";
                BtnGotoOrigin.BackColor = Color.LightGray;
            }
            Origin = CurrentIndexNode(false);
            Origin.Location = new PointF((float)MotionController.GetAxisStatus(XAxis, AxisStatusSignal.PositionFeedback),
                (float)MotionController.GetAxisStatus(YAxis, AxisStatusSignal.PositionFeedback));
            ValidOrigin = true;
            BtnSetOrigin.BackColor = Color.LightGray;
            MakePoints();
            CheckIndexLocation();
        }

        private void BtnOpenGridMaker_Click(object sender, EventArgs e)
        {
            GridComposer.ShowDialog();
            MakePoints();
            CheckIndexLocation();
        }

        private bool MakePoints()
        {
            if (!ValidOrigin) return false;
            Generator = new GridMaker.Generator(Compass.SWpost, Compass.SEpost, Origin);
            Points = Generator.Generate();
            return true;
        }

        private async void BtnGotoOrigin_Click(object sender, EventArgs e)
        {
            if (!ValidOrigin)
            {
                BtnGotoOrigin.Text = "Not Set";
                BtnGotoOrigin.BackColor = Color.Wheat;
            }  
            else
                await Task.Run(() => MovePair(Origin.Location.X, Origin.Location.Y, 15.00, AbsMove, 5000, CTS.Token));
        }

        #region Index Navigator

        private GridMaker.Generator.Node IndexNode;
        private static readonly PointF NullPoint = new PointF(float.MaxValue, float.MaxValue);
        private PointF IndexLocation = NullPoint;

        private GridMaker.Generator.Node CurrentIndexNode(bool zeroIndex)
        {
            int val = zeroIndex ? 1 : 0;
            return new GridMaker.Generator.Node()
            {
                A = new Point((int)NumAX.Value - val, (int)NumAY.Value - val),
                B = new Point((int)NumBX.Value - val, (int)NumBY.Value - val),
                C = new Point((int)NumCX.Value - val, (int)NumCY.Value - val),
            };
        }

        private void CheckIndexLocation()
        {
            IndexNode = CurrentIndexNode(true);
            var matches = Points.Where(x => x.A == IndexNode.A && x.B == IndexNode.B && x.C == IndexNode.C);
            if (matches.Any())
            {
                IndexLocation = Generator.GetStagePosition(matches.First());
                LblXPos.Text = $"X = {IndexLocation.X:f3}";
                LblYPos.Text = $"Y = {IndexLocation.Y:f3}";
            }
            else
            {
                IndexLocation = NullPoint;
                LblXPos.Text = "X = N/A";
                LblYPos.Text = "Y = N/A";
            }
        }

        private void NumABC_ValueChanged(object sender, EventArgs e)
        {
            CheckIndexLocation();
        }

        private async void BtnGotoIndex_Click(object sender, EventArgs e)
        {
            CheckIndexLocation();
            if (IndexLocation != NullPoint)
                await Task.Run(() => MovePair(IndexLocation.X, IndexLocation.Y, 15.00, AbsMove, 5000, CTS.Token));
        }

        private void NodeLabel_Click(object sender, EventArgs e)
        {
            NumAX.Value = 1;
            NumAY.Value = 1;
            NumBX.Value = 1;
            NumBY.Value = 1;
            NumCX.Value = 1;
            NumCY.Value = 1;
            CheckIndexLocation();
        }

        #endregion

        #region Menu Strip Items

        private async void RunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool stamp = ToggleStampInspectionToolStripMenuItem.Checked;
            ImageSaveThreshold = (double)NumImageThreshold.Value;
            try
            {
                bool ready = true;
                if (SEYRCh == null | !ValidOrigin) ready = false;

                if (ready)
                {
                    SEYRCh.ResetAll();
                    MakePoints();
                    bool valid = await Run(stamp);
                }
                else MessageBox.Show("Either load a SEYR project or set a valid SEYR origin and try again.", "SEYR Panel");
            }
            catch (Exception ex) 
            {
                MessageBox.Show($"Invalid setup:\n\n{ex}\n\nTry again", "SEYR Panel");
            }
        }

        private async void ReloadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await SEYRCh.NewImage(LastImage, false, "");
        }

        private async void ForcePatternToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await SEYRCh.NewImage(LastImage, true, "");
        }

        private void ToggleStampInspectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ToggleStampInspectionToolStripMenuItem.Checked) 
                SEYRCh.InputParameters(LastImage);
        }

        private void UseOpticsStageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ValidOrigin = false;
            BtnSetOrigin.BackColor = Color.Wheat;
        }

        private void UpdatePxPerMicronToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (float.TryParse(PxPerMicronToolStripTextBox.Text, out float pxPerMicron))
                SEYRCh.SetPixelsPerMicron(pxPerMicron);
            else
                PxPerMicronToolStripTextBox.Text = SEYRCh.PxPerMicron.ToString();
        }

        private void ChangeDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SEYRCh = null;
            RunToolStripMenuItem.Enabled = false;
            ToolsToolStripMenuItem.Enabled = false;
            BtnOpen.Text = "Open SEYR Directory";
            SEYR.Session.Channel.DiscardViewer();
        }

        private void SaveAsAndOpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog
            {
                Description = "Open a directory to save SEYR project to",
                ShowNewFolderButton = true,
            };
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
                SEYRCh.SaveProjectTo(fbd.SelectedPath);
                SEYRCh = new SEYR.Session.Channel(fbd.SelectedPath);
            }
        }

        #endregion

        #region Callback Control

        private CallbackHandlers CallbackHandler = CallbackHandlers.PF;

        private enum CallbackHandlers
        {
            PF,
            None,
        }

        private bool CallbackPF()
        {
            switch (CallbackHandler)
            {
                case CallbackHandlers.PF:
                    return true;
                default:
                    return false;
            }
        }

        private string[] GetCallbackNames()
        {
            List<string> names = new List<string>();
            foreach (var name in Enum.GetNames(typeof(CallbackHandlers)))
                names.Add(name.Replace("_", " "));
            return names.ToArray();
        }

        private void CallbackToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            CallbackHandler = (CallbackHandlers)Enum.Parse(typeof(CallbackHandlers), CallbackToolStripComboBox.Text);
        }

        #endregion
    }
}
