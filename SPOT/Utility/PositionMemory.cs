﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using static SPOT.Components;
using static SPOT.Functions.Stages;
using static SPOT.Functions.Global;
using MathNet.Spatial.Euclidean;

namespace SPOT.Utility
{
    public partial class PositionMemory
    {
        private FileInfo PosMemoryFile = new FileInfo(@"C:\SPOT\PositionMemory.txt");
        private double[,] PosMemory = new double[3, 3];
        private Button[] PosMemoryBtns = new Button[3];
        private Label[] PosLabels = new Label[3];
        private readonly double EmptyVal = 1000000.0d;
        private bool FirstUse;
        private readonly string Instructions = "Right click to begin editing then click the desired positions in the bottom left of the window.\nRight click the memory button again to save.\nCenter click a memory button to reset it.\nLeft click to go to the saved position.\nPositions are saved for future sessions.";

        public PositionMemory()
        {
            InitPosMemoryUI();
            if (PosMemoryFile.Exists)
            {
                LoadPosMemory();
            }
            else
            {
                FirstUse = true;
                SavePosMemory();
            }

            // Load session information
            if (File.Exists(Compass.StampRegistrationPath))
            {
                try
                {
                    string fileReader = File.ReadAllText(Compass.StampRegistrationPath);
                    string[] data = fileReader.Split('\n');
                    Compass.SWpost = new PointF(float.Parse(data[0]), float.Parse(data[1]));
                    Compass.SEpost = new PointF(float.Parse(data[2]), float.Parse(data[3]));
                    Compass.PostPitch = new PointF(float.Parse(data[4]), float.Parse(data[5]));
                    Compass.NumPosts = new PointF(float.Parse(data[6]), float.Parse(data[7]));
                    Compass.UpdateCenterOfRotation();
                }
                catch (Exception) { }
            }
            else
            {
                FileStream stream = File.Open(Compass.StampRegistrationPath, FileMode.Create, FileAccess.Write);
                WriteToStream(stream, string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n",
                    Compass.SWpost.X, Compass.SWpost.Y, Compass.SEpost.X, Compass.SEpost.Y,
                    Compass.PostPitch.X, Compass.PostPitch.Y, Compass.NumPosts.X, Compass.NumPosts.Y));
                stream.Close();
            }
        }

        private void InitPosMemoryUI()
        {
            PosMemoryBtns = new Button[] { ucMotion.btnPosMem1, ucMotion.btnPosMem2, ucMotion.btnPosMem3 };

            foreach (Button btn in PosMemoryBtns)
            {
                btn.MouseUp += new MouseEventHandler(ClickButton);
            }

            PosLabels = new Label[] { Components.FormMain.lblXpos, Components.FormMain.lblYpos, Components.FormMain.lblZpos };

            for (int i = 0; i < PosLabels.Length; i++)
            {
                PosLabels[i].AccessibleName = "PositionLabel";
                PosLabels[i].AccessibleDescription = i.ToString();
                PosLabels[i].Click += new EventHandler(ClickPosLabel);
            }
        }

        public void ClickPosLabel(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            if (PositionMemoryEditMode)
            {
                if (label.BackColor == Color.Gold)
                    label.BackColor = Color.Transparent; // Allow for deselect
                else
                    label.BackColor = Color.Gold; // Highlight pos if selected to be saved to memory
            }
        }

        private void LoadPosMemory()
        {
            string fileReader = File.ReadAllText(PosMemoryFile.FullName);
            string[] data = fileReader.Split('\n');
            for (int i = 0; i < PosLabels.Length; i++)
            {
                string[] vals = data[i].Split(',');
                double TestPosSum = 0d; // Will be 3 * 1e6 if no pos saved
                for (int j = 0; j < PosLabels.Length; j++)
                {
                    PosMemory[i, j] = double.Parse(vals[j]);
                    TestPosSum += PosMemory[i, j];
                }

                if (TestPosSum != PosLabels.Length * EmptyVal)
                    PosMemoryBtns[i].FlatAppearance.BorderColor = Color.Green;
                else
                    PosMemoryBtns[i].FlatAppearance.BorderColor = SystemColors.ControlDark;
            }
        }

        private void SavePosMemory()
        {
            if (!PosMemoryFile.Directory.Exists)
                Directory.CreateDirectory(PosMemoryFile.Directory.FullName);

            FileStream stream = File.Open(PosMemoryFile.FullName, FileMode.Create, FileAccess.Write);
            for (int i = 0; i < PosLabels.Length; i++)
            {
                for (int j = 0; j < PosLabels.Length; j++)
                {
                    if (FirstUse)
                        PosMemory[i, j] = EmptyVal;
                    WriteToStream(stream, PosMemory[i, j].ToString() + ",");
                }
                WriteToStream(stream, "\n");
            }
            stream.Close();
        }

        private void ClickButton(object mysender, MouseEventArgs e)
        {
            Button sender = (Button)mysender;
            if (FirstUse)
            {
                Interaction.MsgBox(Instructions, Title: "Position Memory --- First Use Instructions");
                FirstUse = false;
                return;
            }

            int MemoryIdx = int.Parse(sender.Text) - 1; // Get array idx from btn
            {
                var withBlock = sender.FlatAppearance; // This color defines the logic
                if (withBlock.BorderColor != Color.Gold & PositionMemoryEditMode)
                    return; // Cannot be editing more than one pos at once
                switch (e.Button)
                {
                    case MouseButtons.Right:
                        {
                            switch (withBlock.BorderColor)
                            {
                                case var @case when @case == SystemColors.ControlDark: // Begin edit
                                    {
                                        withBlock.BorderColor = Color.Gold;
                                        PositionMemoryEditMode = true;
                                        break;
                                    }

                                case var case1 when case1 == Color.Gold: // Finish edit (Store and save values)
                                    {
                                        List<int> usedLabels = new List<int>();
                                        foreach (Label lbl in PosLabels)
                                        {
                                            if (lbl.BackColor == Color.Gold)
                                            {
                                                PosMemory[MemoryIdx, int.Parse(lbl.AccessibleDescription)] = double.Parse(lbl.Text);
                                                usedLabels.Add(int.Parse(lbl.AccessibleDescription));
                                            }
                                            lbl.BackColor = Color.Transparent;
                                        }


                                        // Warn user about saving XY with Z
                                        if (usedLabels.Contains(0) || usedLabels.Contains(1))
                                        {
                                            if (usedLabels.Contains(2))
                                            {
                                                var run = Interaction.MsgBox("It is recommended to save Sample XY and Z moves seperately. Continue anyway?",
                                                    (MsgBoxStyle)((int)MsgBoxStyle.YesNo + (int)MsgBoxStyle.Exclamation), "Position Memory");
                                                if (run == MsgBoxResult.No)
                                                {
                                                    withBlock.BorderColor = SystemColors.ControlDark;
                                                    PositionMemoryEditMode = false;
                                                    break;
                                                }
                                            }
                                        }

                                        // Exit if no labels are selected
                                        if (usedLabels.Count == 0)
                                        {
                                            withBlock.BorderColor = SystemColors.ControlDark;
                                            PositionMemoryEditMode = false;
                                            break;
                                        }

                                        SavePosMemory();
                                        withBlock.BorderColor = Color.Green;
                                        PositionMemoryEditMode = false;
                                        break;
                                    }
                            }

                            break;
                        }

                    case MouseButtons.Middle: // Clear memory pos values and save
                        {
                            if (PositionMemoryEditMode)
                                return;
                            for (int i = 0; i < PosMemoryBtns.Length; i++)
                                PosMemory[MemoryIdx, i] = EmptyVal;
                            withBlock.BorderColor = SystemColors.ControlDark;
                            SavePosMemory();
                            break;
                        }

                    case MouseButtons.Left:
                        {
                            if (PositionMemoryEditMode)
                                return;
                            if (withBlock.BorderColor != Color.Green)
                            {
                                Interaction.MsgBox(Instructions, Title: "Position Memory");
                                return; // Do nothing if there is no saved pos
                            }

                            double x = StatusReporter.Status.X;
                            double y = StatusReporter.Status.Y;
                            double z = StatusReporter.Status.Z;

                            foreach (Label lbl in PosLabels)
                            {
                                double GotoPos = PosMemory[MemoryIdx, int.Parse(lbl.AccessibleDescription)];
                                if (GotoPos != EmptyVal)
                                {
                                    switch (lbl.AccessibleDescription ?? "")
                                    {
                                        case "0":
                                            {
                                                x = GotoPos;
                                                break;
                                            }

                                        case "1":
                                            {
                                                y = GotoPos;
                                                break;
                                            }

                                        case "2":
                                            {
                                                z = GotoPos;
                                                break;
                                            }
                                    }
                                }
                            }

                            Point3D pos = new Point3D(x, y, z);
                            MoveTo3D(pos, CTS.Token);

                            break;
                        }
                }
                ucMotion.Refresh();
            }
        }

        public PointF GetSidecarOffset()
        {
            return new PointF((float)(PosMemory[1, 0] - PosMemory[0, 0]), (float)(PosMemory[1, 1] - PosMemory[0, 1]));
        }
    }
}