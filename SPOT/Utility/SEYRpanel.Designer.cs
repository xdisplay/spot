﻿namespace SPOT.Utility
{
    partial class SEYRpanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SEYRpanel));
            this.TLP = new System.Windows.Forms.TableLayoutPanel();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.NumAX = new System.Windows.Forms.NumericUpDown();
            this.NumAY = new System.Windows.Forms.NumericUpDown();
            this.NumBX = new System.Windows.Forms.NumericUpDown();
            this.NumBY = new System.Windows.Forms.NumericUpDown();
            this.NumCX = new System.Windows.Forms.NumericUpDown();
            this.NumCY = new System.Windows.Forms.NumericUpDown();
            this.LblXPos = new System.Windows.Forms.Label();
            this.LblYPos = new System.Windows.Forms.Label();
            this.LblCompleteTime = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.NumImageThreshold = new System.Windows.Forms.NumericUpDown();
            this.BtnGotoIndex = new System.Windows.Forms.Button();
            this.BtnGotoOrigin = new System.Windows.Forms.Button();
            this.BtnSetOrigin = new System.Windows.Forms.Button();
            this.BtnOpenGridMaker = new System.Windows.Forms.Button();
            this.BtnOpen = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.RunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReloadImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ForcePatternToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToggleStampInspectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveAsAndOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.CallbackToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.PxPerMicronToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.UpdatePxPerMicronToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TLP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumAX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCY)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumImageThreshold)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TLP
            // 
            this.TLP.ColumnCount = 7;
            this.TLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.TLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.TLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.TLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.TLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.TLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.TLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.TLP.Controls.Add(this.ProgressBar, 0, 5);
            this.TLP.Controls.Add(this.label2, 0, 2);
            this.TLP.Controls.Add(this.label3, 1, 2);
            this.TLP.Controls.Add(this.label4, 2, 2);
            this.TLP.Controls.Add(this.label5, 3, 2);
            this.TLP.Controls.Add(this.label6, 4, 2);
            this.TLP.Controls.Add(this.label7, 5, 2);
            this.TLP.Controls.Add(this.NumAX, 0, 3);
            this.TLP.Controls.Add(this.NumAY, 1, 3);
            this.TLP.Controls.Add(this.NumBX, 2, 3);
            this.TLP.Controls.Add(this.NumBY, 3, 3);
            this.TLP.Controls.Add(this.NumCX, 4, 3);
            this.TLP.Controls.Add(this.NumCY, 5, 3);
            this.TLP.Controls.Add(this.LblXPos, 6, 2);
            this.TLP.Controls.Add(this.LblYPos, 6, 3);
            this.TLP.Controls.Add(this.LblCompleteTime, 5, 5);
            this.TLP.Controls.Add(this.flowLayoutPanel1, 5, 0);
            this.TLP.Controls.Add(this.BtnGotoIndex, 4, 0);
            this.TLP.Controls.Add(this.BtnGotoOrigin, 3, 0);
            this.TLP.Controls.Add(this.BtnSetOrigin, 2, 0);
            this.TLP.Controls.Add(this.BtnOpenGridMaker, 1, 0);
            this.TLP.Controls.Add(this.BtnOpen, 0, 0);
            this.TLP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TLP.Location = new System.Drawing.Point(0, 24);
            this.TLP.Name = "TLP";
            this.TLP.RowCount = 6;
            this.TLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.TLP.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TLP.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.TLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TLP.Size = new System.Drawing.Size(659, 152);
            this.TLP.TabIndex = 0;
            // 
            // ProgressBar
            // 
            this.TLP.SetColumnSpan(this.ProgressBar, 5);
            this.ProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProgressBar.Location = new System.Drawing.Point(3, 135);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(464, 14);
            this.ProgressBar.Step = 1;
            this.ProgressBar.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(3, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 15);
            this.label2.TabIndex = 12;
            this.label2.Text = "RR";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.NodeLabel_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(97, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 15);
            this.label3.TabIndex = 13;
            this.label3.Text = "RC";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.NodeLabel_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(191, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 15);
            this.label4.TabIndex = 14;
            this.label4.Text = "R";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Click += new System.EventHandler(this.NodeLabel_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(285, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 15);
            this.label5.TabIndex = 15;
            this.label5.Text = "C";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.NodeLabel_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(379, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 15);
            this.label6.TabIndex = 16;
            this.label6.Text = "SR";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Click += new System.EventHandler(this.NodeLabel_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(473, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 15);
            this.label7.TabIndex = 17;
            this.label7.Text = "SC";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.NodeLabel_Click);
            // 
            // NumAX
            // 
            this.NumAX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumAX.Location = new System.Drawing.Point(3, 99);
            this.NumAX.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NumAX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumAX.Name = "NumAX";
            this.NumAX.Size = new System.Drawing.Size(88, 20);
            this.NumAX.TabIndex = 19;
            this.NumAX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumAX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumAX.ValueChanged += new System.EventHandler(this.NumABC_ValueChanged);
            // 
            // NumAY
            // 
            this.NumAY.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumAY.Location = new System.Drawing.Point(97, 99);
            this.NumAY.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NumAY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumAY.Name = "NumAY";
            this.NumAY.Size = new System.Drawing.Size(88, 20);
            this.NumAY.TabIndex = 20;
            this.NumAY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumAY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumAY.ValueChanged += new System.EventHandler(this.NumABC_ValueChanged);
            // 
            // NumBX
            // 
            this.NumBX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumBX.Location = new System.Drawing.Point(191, 99);
            this.NumBX.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NumBX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBX.Name = "NumBX";
            this.NumBX.Size = new System.Drawing.Size(88, 20);
            this.NumBX.TabIndex = 21;
            this.NumBX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumBX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBX.ValueChanged += new System.EventHandler(this.NumABC_ValueChanged);
            // 
            // NumBY
            // 
            this.NumBY.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumBY.Location = new System.Drawing.Point(285, 99);
            this.NumBY.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NumBY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBY.Name = "NumBY";
            this.NumBY.Size = new System.Drawing.Size(88, 20);
            this.NumBY.TabIndex = 22;
            this.NumBY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumBY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBY.ValueChanged += new System.EventHandler(this.NumABC_ValueChanged);
            // 
            // NumCX
            // 
            this.NumCX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumCX.Location = new System.Drawing.Point(379, 99);
            this.NumCX.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NumCX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumCX.Name = "NumCX";
            this.NumCX.Size = new System.Drawing.Size(88, 20);
            this.NumCX.TabIndex = 23;
            this.NumCX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumCX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumCX.ValueChanged += new System.EventHandler(this.NumABC_ValueChanged);
            // 
            // NumCY
            // 
            this.NumCY.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumCY.Location = new System.Drawing.Point(473, 99);
            this.NumCY.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NumCY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumCY.Name = "NumCY";
            this.NumCY.Size = new System.Drawing.Size(88, 20);
            this.NumCY.TabIndex = 24;
            this.NumCY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumCY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumCY.ValueChanged += new System.EventHandler(this.NumABC_ValueChanged);
            // 
            // LblXPos
            // 
            this.LblXPos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblXPos.AutoSize = true;
            this.LblXPos.Location = new System.Drawing.Point(567, 81);
            this.LblXPos.Name = "LblXPos";
            this.LblXPos.Size = new System.Drawing.Size(89, 15);
            this.LblXPos.TabIndex = 25;
            this.LblXPos.Text = "X = N/A";
            this.LblXPos.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // LblYPos
            // 
            this.LblYPos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblYPos.AutoSize = true;
            this.LblYPos.Location = new System.Drawing.Point(567, 96);
            this.LblYPos.Name = "LblYPos";
            this.LblYPos.Size = new System.Drawing.Size(89, 26);
            this.LblYPos.TabIndex = 26;
            this.LblYPos.Text = "Y = N/A";
            // 
            // LblCompleteTime
            // 
            this.LblCompleteTime.AutoSize = true;
            this.TLP.SetColumnSpan(this.LblCompleteTime, 2);
            this.LblCompleteTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblCompleteTime.Location = new System.Drawing.Point(473, 132);
            this.LblCompleteTime.Name = "LblCompleteTime";
            this.LblCompleteTime.Size = new System.Drawing.Size(183, 20);
            this.LblCompleteTime.TabIndex = 27;
            this.LblCompleteTime.Text = "Complete at N/A";
            this.LblCompleteTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoSize = true;
            this.TLP.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.NumImageThreshold);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(473, 9);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(183, 52);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Image Capture: Threshold for failing features % as decimal (-1 for off)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NumImageThreshold
            // 
            this.NumImageThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.NumImageThreshold.DecimalPlaces = 2;
            this.NumImageThreshold.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NumImageThreshold.Location = new System.Drawing.Point(45, 29);
            this.NumImageThreshold.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumImageThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.NumImageThreshold.Name = "NumImageThreshold";
            this.NumImageThreshold.Size = new System.Drawing.Size(89, 20);
            this.NumImageThreshold.TabIndex = 1;
            this.NumImageThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BtnGotoIndex
            // 
            this.BtnGotoIndex.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnGotoIndex.AutoSize = true;
            this.BtnGotoIndex.BackColor = System.Drawing.Color.LightGray;
            this.BtnGotoIndex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGotoIndex.Location = new System.Drawing.Point(379, 3);
            this.BtnGotoIndex.Name = "BtnGotoIndex";
            this.BtnGotoIndex.Size = new System.Drawing.Size(88, 65);
            this.BtnGotoIndex.TabIndex = 11;
            this.BtnGotoIndex.Text = "Goto Index";
            this.BtnGotoIndex.UseVisualStyleBackColor = false;
            this.BtnGotoIndex.Click += new System.EventHandler(this.BtnGotoIndex_Click);
            // 
            // BtnGotoOrigin
            // 
            this.BtnGotoOrigin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnGotoOrigin.AutoSize = true;
            this.BtnGotoOrigin.BackColor = System.Drawing.Color.LightGray;
            this.BtnGotoOrigin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGotoOrigin.Location = new System.Drawing.Point(285, 3);
            this.BtnGotoOrigin.Name = "BtnGotoOrigin";
            this.BtnGotoOrigin.Size = new System.Drawing.Size(88, 65);
            this.BtnGotoOrigin.TabIndex = 10;
            this.BtnGotoOrigin.Text = "Goto Origin";
            this.BtnGotoOrigin.UseVisualStyleBackColor = false;
            this.BtnGotoOrigin.Click += new System.EventHandler(this.BtnGotoOrigin_Click);
            // 
            // BtnSetOrigin
            // 
            this.BtnSetOrigin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSetOrigin.AutoSize = true;
            this.BtnSetOrigin.BackColor = System.Drawing.Color.Wheat;
            this.BtnSetOrigin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSetOrigin.Location = new System.Drawing.Point(191, 3);
            this.BtnSetOrigin.Name = "BtnSetOrigin";
            this.BtnSetOrigin.Size = new System.Drawing.Size(88, 65);
            this.BtnSetOrigin.TabIndex = 7;
            this.BtnSetOrigin.Text = "Set Origin";
            this.BtnSetOrigin.UseVisualStyleBackColor = false;
            this.BtnSetOrigin.Click += new System.EventHandler(this.BtnSetOrigin_Click);
            // 
            // BtnOpenGridMaker
            // 
            this.BtnOpenGridMaker.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOpenGridMaker.AutoSize = true;
            this.BtnOpenGridMaker.BackColor = System.Drawing.Color.LightGray;
            this.BtnOpenGridMaker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnOpenGridMaker.Location = new System.Drawing.Point(97, 3);
            this.BtnOpenGridMaker.Name = "BtnOpenGridMaker";
            this.BtnOpenGridMaker.Size = new System.Drawing.Size(88, 65);
            this.BtnOpenGridMaker.TabIndex = 8;
            this.BtnOpenGridMaker.Text = "Open Grid Maker";
            this.BtnOpenGridMaker.UseVisualStyleBackColor = false;
            this.BtnOpenGridMaker.Click += new System.EventHandler(this.BtnOpenGridMaker_Click);
            // 
            // BtnOpen
            // 
            this.BtnOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnOpen.AutoSize = true;
            this.BtnOpen.BackColor = System.Drawing.Color.White;
            this.BtnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnOpen.Location = new System.Drawing.Point(3, 3);
            this.BtnOpen.Name = "BtnOpen";
            this.BtnOpen.Size = new System.Drawing.Size(88, 65);
            this.BtnOpen.TabIndex = 0;
            this.BtnOpen.Text = "Open SEYR Directory";
            this.BtnOpen.UseVisualStyleBackColor = false;
            this.BtnOpen.Click += new System.EventHandler(this.BtnOpen_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RunToolStripMenuItem,
            this.ToolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(659, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // RunToolStripMenuItem
            // 
            this.RunToolStripMenuItem.AutoSize = false;
            this.RunToolStripMenuItem.BackColor = System.Drawing.Color.LightBlue;
            this.RunToolStripMenuItem.Enabled = false;
            this.RunToolStripMenuItem.Name = "RunToolStripMenuItem";
            this.RunToolStripMenuItem.Size = new System.Drawing.Size(150, 20);
            this.RunToolStripMenuItem.Text = "Run";
            this.RunToolStripMenuItem.Click += new System.EventHandler(this.RunToolStripMenuItem_Click);
            // 
            // ToolsToolStripMenuItem
            // 
            this.ToolsToolStripMenuItem.AutoSize = false;
            this.ToolsToolStripMenuItem.BackColor = System.Drawing.Color.Thistle;
            this.ToolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReloadImageToolStripMenuItem,
            this.ForcePatternToolStripMenuItem,
            this.ToggleStampInspectionToolStripMenuItem,
            this.ChangeDirectoryToolStripMenuItem,
            this.SaveAsAndOpenToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.CallbackToolStripComboBox,
            this.toolStripSeparator2,
            this.toolStripTextBox2,
            this.PxPerMicronToolStripTextBox,
            this.UpdatePxPerMicronToolStripMenuItem});
            this.ToolsToolStripMenuItem.Enabled = false;
            this.ToolsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem";
            this.ToolsToolStripMenuItem.Size = new System.Drawing.Size(150, 20);
            this.ToolsToolStripMenuItem.Text = "Tools";
            // 
            // ReloadImageToolStripMenuItem
            // 
            this.ReloadImageToolStripMenuItem.Name = "ReloadImageToolStripMenuItem";
            this.ReloadImageToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ReloadImageToolStripMenuItem.Text = "Reload Image";
            this.ReloadImageToolStripMenuItem.Click += new System.EventHandler(this.ReloadImageToolStripMenuItem_Click);
            // 
            // ForcePatternToolStripMenuItem
            // 
            this.ForcePatternToolStripMenuItem.Name = "ForcePatternToolStripMenuItem";
            this.ForcePatternToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ForcePatternToolStripMenuItem.Text = "Force Pattern";
            this.ForcePatternToolStripMenuItem.Click += new System.EventHandler(this.ForcePatternToolStripMenuItem_Click);
            // 
            // ToggleStampInspectionToolStripMenuItem
            // 
            this.ToggleStampInspectionToolStripMenuItem.CheckOnClick = true;
            this.ToggleStampInspectionToolStripMenuItem.Name = "ToggleStampInspectionToolStripMenuItem";
            this.ToggleStampInspectionToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ToggleStampInspectionToolStripMenuItem.Text = "Toggle Stamp Inspection";
            this.ToggleStampInspectionToolStripMenuItem.Click += new System.EventHandler(this.ToggleStampInspectionToolStripMenuItem_Click);
            // 
            // ChangeDirectoryToolStripMenuItem
            // 
            this.ChangeDirectoryToolStripMenuItem.Name = "ChangeDirectoryToolStripMenuItem";
            this.ChangeDirectoryToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ChangeDirectoryToolStripMenuItem.Text = "Change Directory";
            this.ChangeDirectoryToolStripMenuItem.Click += new System.EventHandler(this.ChangeDirectoryToolStripMenuItem_Click);
            // 
            // SaveAsAndOpenToolStripMenuItem
            // 
            this.SaveAsAndOpenToolStripMenuItem.Name = "SaveAsAndOpenToolStripMenuItem";
            this.SaveAsAndOpenToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.SaveAsAndOpenToolStripMenuItem.Text = "Save As and Open";
            this.SaveAsAndOpenToolStripMenuItem.Click += new System.EventHandler(this.SaveAsAndOpenToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(202, 6);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "Callback Handler";
            // 
            // CallbackToolStripComboBox
            // 
            this.CallbackToolStripComboBox.Name = "CallbackToolStripComboBox";
            this.CallbackToolStripComboBox.Size = new System.Drawing.Size(121, 23);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(202, 6);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox2.Text = "Pixels per Micron";
            // 
            // PxPerMicronToolStripTextBox
            // 
            this.PxPerMicronToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.PxPerMicronToolStripTextBox.Name = "PxPerMicronToolStripTextBox";
            this.PxPerMicronToolStripTextBox.Size = new System.Drawing.Size(100, 23);
            // 
            // UpdatePxPerMicronToolStripMenuItem
            // 
            this.UpdatePxPerMicronToolStripMenuItem.Name = "UpdatePxPerMicronToolStripMenuItem";
            this.UpdatePxPerMicronToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.UpdatePxPerMicronToolStripMenuItem.Text = "Update";
            this.UpdatePxPerMicronToolStripMenuItem.Click += new System.EventHandler(this.UpdatePxPerMicronToolStripMenuItem_Click);
            // 
            // SEYRpanel
            // 
            this.AccessibleName = "SEYRpanel";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 176);
            this.Controls.Add(this.TLP);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(675, 215);
            this.Name = "SEYRpanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SEYR Control Panel";
            this.TLP.ResumeLayout(false);
            this.TLP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumAX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCY)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumImageThreshold)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TLP;
        private System.Windows.Forms.Button BtnOpen;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.Button BtnSetOrigin;
        private System.Windows.Forms.Button BtnOpenGridMaker;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NumImageThreshold;
        private System.Windows.Forms.Button BtnGotoOrigin;
        private System.Windows.Forms.Button BtnGotoIndex;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown NumAX;
        private System.Windows.Forms.NumericUpDown NumAY;
        private System.Windows.Forms.NumericUpDown NumBX;
        private System.Windows.Forms.NumericUpDown NumBY;
        private System.Windows.Forms.NumericUpDown NumCX;
        private System.Windows.Forms.NumericUpDown NumCY;
        private System.Windows.Forms.Label LblXPos;
        private System.Windows.Forms.Label LblYPos;
        private System.Windows.Forms.Label LblCompleteTime;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem RunToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReloadImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ForcePatternToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToggleStampInspectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripComboBox CallbackToolStripComboBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripTextBox PxPerMicronToolStripTextBox;
        private System.Windows.Forms.ToolStripMenuItem UpdatePxPerMicronToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChangeDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveAsAndOpenToolStripMenuItem;
    }
}