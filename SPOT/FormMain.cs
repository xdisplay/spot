﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using static SPOT.Components;
using static SPOT.Functions.Global;
using static SPOT.Functions.Stages;

namespace SPOT
{
    public partial class FormMain : Form
    {
        public static string UIMemoryPath = @"C:\SPOT\UIMemory.txt";

        public FormMain()
        {
            InitializeComponent();
            LoadUI();
            Setup(this);
        }

        private void LoadUI()
        {
            if (File.Exists(UIMemoryPath))
            {
                string[] lines = File.ReadAllLines(UIMemoryPath);
                if (int.TryParse(lines[0], out int whiteLED)) NumWhiteLED.Value = whiteLED;
                if (int.TryParse(lines[1], out int redLED)) NumRedLED.Value = redLED;
            }
        }

        private void SaveUI()
        {
            File.WriteAllText(UIMemoryPath, $"{NumWhiteLED.Value}\n{NumRedLED.Value}");
        }

        private void ToolStripButtonReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void BtnHomeAllStages_Click(object sender, EventArgs e)
        {
            Home();
        }

        private async void BtnToCenter_Click(object sender, EventArgs e)
        {
            await ToCenter();
        }

        private async void btnToLoad_Click(object sender, EventArgs e)
        {
            await ToLoad();
        }

        private async void ShuttleToSensorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await Shuttle();
        }

        private async void shuttleToCameraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await Shuttle(toSensor: false);
        }

        private void ToolStripButtonOpenCompass_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Compass>().Any())
                Application.OpenForms.OfType<Compass>().First().BringToFront();
            else
                _ = new Compass();
        }

        private void ToolStripButtonRegistration_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Registration>().Any())
                Application.OpenForms.OfType<Registration>().First().BringToFront();
            else
                _ = new Registration();
        }

        private void ExportPlotDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utility.Plotter.ExportData();
        }

        private void ResetPlotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utility.Plotter.InitializePlot();
        }

        private void toolStripButtonSwitchFeed_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            ConfocalDovetail = !ConfocalDovetail;
            if (ConfocalDovetail)
            {
                ComponentVimba.Disconnect();
                ComponentCamera.Initialize();
            }
            else
            {
                ComponentCamera.Disconnect();
                ComponentVimba.Initialize();
            }
            Cursor = Cursors.Default;
        }

        private async void WaferScanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await Scans.Wafer.Run(int.Parse(((ToolStripMenuItem)sender).Tag.ToString()));
        }

        private async void stampScanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await Scans.Stamp.Run();
        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Documentation.FormMain>().Any())
                Application.OpenForms.OfType<Documentation.FormMain>().First().BringToFront();
            else
                _ = new Documentation.FormMain();
        }

        private void toolStripButtonShowZoom_Click(object sender, EventArgs e)
        {
            Zoom = !Zoom;
        }

        private void scanParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScanParameters.Show();
            ScanParameters.BringToFront();
        }

        private void archiveLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] paths = new string[] { 
                @"C:\SPOT\Data\Log\HeightSensorLog.txt", 
                @"C:\SPOT\Data\Log\Archive\",
                @"\\XDISPLAY-FS01.x-display.local\Company\Systems Eng\SPOT Network Folder\HeightSensorLog.txt",
                @"\\XDISPLAY-FS01.x-display.local\Company\Systems Eng\SPOT Network Folder\Archive\" };

            foreach (string path in paths)
            {
                if (path.EndsWith(".txt"))
                {
                    if (!File.Exists(path))
                    {
                        MessageBox.Show($"{path} not found. Archive aborted.");
                        return;
                    }
                }
                else
                {
                    Directory.CreateDirectory(path);
                }
            }

            string dt = DateTime.Now.ToString("ddMMMyyyy-HH_mm_ss").ToUpper();
            try
            {
                File.Move(paths[0], $"{paths[1]}{dt}.txt");
                File.Move(paths[2], $"{paths[3]}{dt}.txt");
                File.Delete(paths[0]);
                File.Delete(paths[2]);
                File.Create(paths[0]);
            }
            catch (Exception)
            {
                MessageBox.Show("Archive failed");
                return;
            }
            MessageBox.Show("Archive completed");
        }

        private void calibrateSidecamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Saved position 1 = camera corner?\nSaved position 2 = height sensor corner?", "Sidecam Calibration", MessageBoxButtons.YesNo);
            try
            {
                if (result == DialogResult.Yes)
                {
                    ComponentHeightSensor.HeightSensePos = PositionMemory.GetSidecarOffset();
                    MessageBox.Show($"Calibration complete. New offset: {ComponentHeightSensor.HeightSensePos}");
                }
            }
            catch (Exception)
            {
                MessageBox.Show($"Calibration failed.");
            }
        }

        private void NumWhiteLED_ValueChanged(object sender, EventArgs e)
        {
            ComponentLEDController.Set(2, NumWhiteLED.Value);
            SaveUI();
        }

        private void NumRedLED_ValueChanged(object sender, EventArgs e)
        {
            ComponentLEDController.Set(3, NumRedLED.Value);
            SaveUI();
        }

        private void NumZoom_ValueChanged(object sender, EventArgs e)
        {
            Utility.ZoomImage.Percentage = (double)NumZoom.Value;
        }

        private void SEYRtoolStripButton_Click(object sender, EventArgs e)
        {
            if (SEYRpanel == null)
                SEYRpanel = new Utility.SEYRpanel();
            SEYRpanel.Show();
        }
    }
}
