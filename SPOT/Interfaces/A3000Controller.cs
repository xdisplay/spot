﻿using Aerotech.A3200;
using Aerotech.A3200.Commands;
using Aerotech.A3200.DataCollection;
using Aerotech.A3200.Parameters;
using Aerotech.A3200.Status;
using Aerotech.A3200.Tasks;
using System;

namespace SPOT.Interfaces
{
    public class A3000Controller : IController
    {
        private Controller _controller;

        public Controller GetController()
        {
            return _controller;
        }

        public void SetOutput(int bit, int axis, int value)
        {
            _controller.Commands.IO.DigitalOutputBit(bit, axis, value);
        }

        public int GetDigitalInputBit(short bit, int axis)
        {
            return _controller.Commands.IO.DigitalInputBit(bit, axis);
        }

        public void Connect()
        {
            _controller = Controller.Connect();
            _controller.ControlCenter.Diagnostics.NewDiagPacketArrived += Diagnostics_NewDiagPacketArrived;
        }

        public event EventHandler<NewDiagPacketArrivedEventArgs> DiagnosticsPacketArrived;

        private void Diagnostics_NewDiagPacketArrived(object sender, NewDiagPacketArrivedEventArgs e)
        {
            DiagnosticsPacketArrived?.Invoke(this, e);
        }

        public ControllerDiagPacket RetrieveDiagnostics()
        {
            return _controller.DataCollection.RetrieveDiagnostics();
        }

        public void AcknowledgeAll()
        {
            _controller.Commands.AcknowledgeAll();
        }

        public void EnableMotion(AxisMask axisMask)
        {
            _controller.Commands.Motion.Enable(axisMask);
        }

        public void AbortMotion(AxisMask axisMask)
        {
            _controller.Commands.Motion.Abort(axisMask);
        }

        public TaskState GetTaskState(int i)
        {
            return _controller.Tasks[i].State;
        }

        public Task GetTask(int i)
        {
            return _controller.Tasks[i];
        }

        public void StartProgram(int i)
        {
            _controller.Tasks[i].Program.Start();
        }

        public void StopProgram(int i)
        {
            _controller.Tasks[i].Program.Stop();
        }

        public string GetControllerMajorVersion()
        {
            return _controller.Information.Version.SMCVersion.Major.ToString();
        }

        public string GetControllerMinorVersion()
        {
            return _controller.Information.Version.SMCVersion.Minor.ToString("00");
        }

        public void FaultAck(int i)
        {
            _controller.Commands.Axes[i].Motion.FaultAck();
        }

        public void MotionFaultAck(AxisMask axis)
        {
            _controller.Commands.Motion.FaultAck(axis);
        }

        public void MotionDisable(AxisMask axis)
        {
            _controller.Commands.Motion.Disable(axis);
        }

        public double GetValue(int i)
        {
            return _controller.Variables.Global.Doubles[i].Value;
        }

        public void SetValue(int i, double value)
        {
            _controller.Variables.Global.Doubles[i].Value = value;
        }

        public void FreeRun(int axis, double velocity)
        {
            _controller.Commands.Motion.FreeRun(axis, velocity);
        }

        public void MoveAbs(int axis, double position, double speed)
        {
            _controller.Commands.Motion.MoveAbs(axis, position, speed);
        }

        public void MoveInc(int axis, double position, double speed)
        {
            _controller.Commands.Motion.MoveInc(axis, position, speed);
        }

        public bool WaitForMotionDone(WaitOption waitOption, int axisIndex, int timeout)
        {
            return _controller.Commands.Motion.WaitForMotionDone(waitOption, axisIndex, timeout);
        }

        public void WaitForMotionDone(WaitOption waitOption, int axisIndex)
        {
            _controller.Commands.Motion.WaitForMotionDone(waitOption, axisIndex);
        }

        public bool WaitForMotionDoneInAxis(int xAxis, int yAxis, WaitOption waitOption, int timeout)
        {
            return _controller.Commands.Axes[xAxis, yAxis].Motion.WaitForMotionDone(WaitOption.InPosition, timeout);
        }

        public StatusCommands GetTaskStatus(int i)
        {
            return _controller.Commands[TaskId.T03].Status;
        }

        public void SetupAbsolute()
        {
            _controller.Commands.Motion.Setup.Absolute();
        }

        public void SetupIncremental()
        {
            _controller.Commands.Motion.Setup.Incremental();
        }

        public void MotionLinear(int xAxis, int yAxis, double[] Distance, double CoordinatedSpeed)
        {
            _controller.Commands.Axes[xAxis, yAxis].Motion.Linear(Distance, CoordinatedSpeed);
        }

        public void FreeRunStop(int axis)
        {
            _controller.Commands.Motion.FreeRunStop(axis);
        }

        public void GoHome(int axis)
        {
            _controller.Commands.Motion.Home(axis);
        }

        public double GetAxisStatus(int axis, AxisStatusSignal status)
        {
            return _controller.Commands.Status.AxisStatus(axis, status);
        }

        public ControllerAxisParameterCategory GetParametersAxes(int axis)
        {
            return _controller.Parameters.Axes[axis];
        }

        public Program GetProgram(int task)
        {
            return _controller.Tasks[task].Program;
        }

        public DataCollectionStatus GetDataCollectionStatus()
        {
            return _controller.DataCollection.Status;
        }

        public void DataCollectionStop()
        {
            _controller.DataCollection.Stop();
        }

        public void DataCollectionClearConfiguration()
        {
            _controller.DataCollection.Configuration.Clear();
        }

        public DataCollectionConfiguration GetDataCollectionConfiguration()
        {
            return _controller.DataCollection.Configuration;
        }

        public void StartDataCollection()
        {
            _controller.DataCollection.Start();
        }

        public void SetVariablesInput(int i, int value)
        {
            _controller.Variables.VirtualIO.Binary.Inputs[i].Value = Convert.ToBoolean(value);
        }

        public DataCollectionResults GetDatacollectionResult(int i)
        {
            return _controller.DataCollection.GetData(i);
        }

        public AxesRootCommands SelectAxes(AxisMask[] axisMask)
        {
            return _controller.Commands.Axes.Select(axisMask);
        }

        public void Execute(string message)
        {
            _controller.Commands.Execute(message);
        }

        public void Disconnect()
        {
            Controller.Disconnect();
        }
    }
}