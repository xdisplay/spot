﻿using Aerotech.A3200;
using Aerotech.A3200.Commands;
using Aerotech.A3200.DataCollection;
using Aerotech.A3200.Parameters;
using Aerotech.A3200.Status;
using Aerotech.A3200.Tasks;
using System;

namespace SPOT.Interfaces
{
    public interface IController
    {
        void SetOutput(int bit, int axis, int value);
        int GetDigitalInputBit(short bit, int axist);
        void Connect();
        ControllerDiagPacket RetrieveDiagnostics();
        void AcknowledgeAll();
        void EnableMotion(AxisMask axisMask);
        void AbortMotion(AxisMask axisMask);
        TaskState GetTaskState(int i);
        Task GetTask(int i);
        void StartProgram(int i);
        void StopProgram(int i);
        // Function GetControllerInformation() As ControllerInformation
        string GetControllerMajorVersion();
        string GetControllerMinorVersion();
        void FaultAck(int i);
        void MotionFaultAck(AxisMask axis);
        void MotionDisable(AxisMask axis);
        double GetValue(int i);
        void SetValue(int i, double value);
        void FreeRun(int axis, double velocity);
        void MoveAbs(int axis, double position, double speed);
        void MoveInc(int axis, double position, double speed);
        bool WaitForMotionDone(WaitOption waitOption, int axisIndex, int timeout);
        void WaitForMotionDone(WaitOption waitOption, int axisIndex);
        bool WaitForMotionDoneInAxis(int xAxis, int yAxis, WaitOption waitOption, int timeout);
        StatusCommands GetTaskStatus(int i);
        void SetupAbsolute();
        void SetupIncremental();
        void MotionLinear(int xAxis, int yAxis, double[] Distance, double CoordinatedSpeed);
        void FreeRunStop(int axis);
        void GoHome(int axis);
        double GetAxisStatus(int axis, AxisStatusSignal status);
        ControllerAxisParameterCategory GetParametersAxes(int axis);
        Program GetProgram(int task);
        DataCollectionStatus GetDataCollectionStatus();
        void DataCollectionStop();
        void DataCollectionClearConfiguration();
        DataCollectionConfiguration GetDataCollectionConfiguration();
        void StartDataCollection();
        void SetVariablesInput(int i, int value);
        DataCollectionResults GetDatacollectionResult(int i);
        AxesRootCommands SelectAxes(AxisMask[] axisMask);
        void Execute(string message);
        void Disconnect();

        event EventHandler<NewDiagPacketArrivedEventArgs> DiagnosticsPacketArrived;
        // need handler
    }
}