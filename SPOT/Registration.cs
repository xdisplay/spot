﻿using Aerotech.A3200.Status;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using static SPOT.Components;
using static SPOT.Functions.Global;
using static SPOT.Functions.Stages;

namespace SPOT
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
            numPitchX.Value = (decimal)(Compass.PostPitch.X * 1e3);
            numPitchY.Value = (decimal)(Compass.PostPitch.Y * 1e3);
            numDevicesX.Value = (decimal)Compass.NumPosts.X;
            numDevicesY.Value = (decimal)Compass.NumPosts.Y;
            Show();
        }

        private void btnSaveSW_Click(object sender, EventArgs e)
        {
            Compass.SWpost = new PointF((float)MotionController.GetAxisStatus(XAxis, AxisStatusSignal.PositionFeedback),
                (float)MotionController.GetAxisStatus(YAxis, AxisStatusSignal.PositionFeedback));
        }

        private void btnSaveSE_Click(object sender, EventArgs e)
        {
            Compass.SEpost = new PointF((float)MotionController.GetAxisStatus(XAxis, AxisStatusSignal.PositionFeedback),
                (float)MotionController.GetAxisStatus(YAxis, AxisStatusSignal.PositionFeedback));
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            Compass.PostPitch = new PointF((float)numPitchX.Value / 1e3f, (float)numPitchY.Value / 1e3f);
            Compass.NumPosts = new PointF((float)numDevicesX.Value, (float)numDevicesY.Value);

            FileStream stream = File.Open(Compass.StampRegistrationPath, FileMode.Create, FileAccess.Write);
            WriteToStream(stream, string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n",
                Compass.SWpost.X, Compass.SWpost.Y, Compass.SEpost.X, Compass.SEpost.Y,
                Compass.PostPitch.X, Compass.PostPitch.Y, Compass.NumPosts.X, Compass.NumPosts.Y));
            stream.Close();

            Compass.UpdateCenterOfRotation();

            Close();
        }
    }
}
