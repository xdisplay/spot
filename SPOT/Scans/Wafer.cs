﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using static SPOT.Functions.Global;
using static SPOT.Functions.Stages;

namespace SPOT.Scans
{
    public static class Wafer
    {
        public static async Task Run(int diameter)
        {
            // Starts from center
            // TODO determine wafer center with height sensor
            await ToCenter();
            //await FindSensorPlane(CTS.Token);
            List<PointF> scanPoints = GetWaferScanPoints(diameter - 3);

            Components.FormMain.BeginInvoke((MethodInvoker)delegate ()
            {
                Components.FormMain.toolStripProgressBar.Visible = true;
                Components.FormMain.toolStripProgressBar.Value = 0;
                Components.FormMain.toolStripProgressBar.Maximum = scanPoints.Count;
            });

            //await Task.Run(() => FindSensorPlane(CTS.Token));

            Utility.Plotter.InitializePlot(collectByDefault: false);

            foreach (PointF point in scanPoints)
            {
                await Task.Run(() => MovePair(point.X, point.Y, Components.ScanParameters.ScanSpeed, AbsMove, 10000, CTS.Token));
                Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.toolStripProgressBar.PerformStep(); });
                Utility.Plotter.Collect = true;
                System.Threading.Thread.SpinWait(100);
            }

            Utility.Plotter.Collect = false;

            Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.toolStripProgressBar.Visible = false; });

            if (!CTS.IsCancellationRequested) Utility.Plotter.ExportData();
        }

        private static List<PointF> GetWaferScanPoints(int diameter)
        {
            PointF center = new PointF(StageRange.X / 2, StageRange.Y / 2);
            List<PointF> points = new List<PointF>();
            double radius = diameter / 3;
            double ySkip = diameter / (double)Components.ScanParameters.NumPasses;
            for (double i = center.Y + radius; i >= center.Y - radius; i -= ySkip * 2)
            {
                double y = i; if (y > StageRange.Y) y = 200; else if (y < 0) y = 0;
                double x = Math.Sqrt(Math.Pow(radius, 2) - Math.Pow(y - center.Y, 2)) + center.X;
                if (double.IsNaN(x)) continue; else if (x > StageRange.X) x = 200; else if (x < 0) x = 0;           
                points.Add(new PointF((float)x, (float)y));
                points.Add(new PointF(Math.Abs(StageRange.X - (float)x), (float)y));
                x = Math.Sqrt(Math.Pow(radius, 2) - Math.Pow(y - ySkip - center.Y, 2)) + center.X;
                if (double.IsNaN(x)) continue; else if (x > StageRange.X) x = 200; else if (x < 0) x = 0;
                points.Add(new PointF(Math.Abs(StageRange.X - (float)x), (float)(y - ySkip)));
                points.Add(new PointF((float)x, (float)(y - ySkip)));

            }
            return points;
        }

        private static List<PointF> GetPerimeter(float OuterCircleRadius)
        {
            PointF Center = new PointF(StageRange.X / 2, StageRange.Y / 2);
            List<PointF> pt = new List<PointF>();

            float deg = 20;
            double rU = OuterCircleRadius;
            double z = deg / 180.0 * Math.PI;

            double pt1X = Center.X + rU;
            double pt1Y = Center.Y;

            pt.Add(new PointF(Convert.ToSingle(pt1X), Convert.ToSingle(pt1Y)));

            int j = 1;

            for (double i = z; i <= Math.PI * 2.0; i += z)
            {
                double ptX = ((pt1X - Center.X) * Math.Cos(i)) + ((pt1Y - Center.Y) * -Math.Sin(i)) + Center.X;
                double ptY = ((pt1X - Center.X) * Math.Sin(i)) + ((pt1Y - Center.Y) * Math.Cos(i)) + Center.Y;
                pt.Add(new PointF((float)ptX, (float)ptY));
                j += 1;
            }

            return pt;
        }
    }
}
