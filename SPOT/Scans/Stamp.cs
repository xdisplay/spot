﻿using System.Threading.Tasks;
using System.Windows.Forms;
using static SPOT.Functions.Global;

namespace SPOT.Scans
{
    class Stamp
    {
        public static async Task Run()
        {
            if (Components.ScanParameters.NumPasses > Compass.NumPosts.X) Components.ScanParameters.NumPasses = (int)Compass.NumPosts.X;
            float step = (int)(Compass.NumPosts.X / (double)(Components.ScanParameters.NumPasses - 1));

            Components.FormMain.BeginInvoke((MethodInvoker)delegate ()
            {
                Components.FormMain.toolStripProgressBar.Visible = true;
                Components.FormMain.toolStripProgressBar.Value = 0;
                Components.FormMain.toolStripProgressBar.Maximum = (int)(Compass.NumPosts.X / step);
            });

            Utility.Plotter.InitializePlot(collectByDefault: false);

            float scanPos = 0.0f;
            while (scanPos < Compass.NumPosts.X)
            {
                await Compass.ToStampPost("Index", scanPos, 0, true, CTS.Token);
                Utility.Plotter.Collect = true;
                await Compass.ToStampPost("Index", scanPos, (int)(Compass.NumPosts.Y - 1), true, CTS.Token, speed: Components.ScanParameters.ScanSpeed);
                Utility.Plotter.Collect = false;
                Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.toolStripProgressBar.PerformStep(); });

                await Compass.ToStampPost("Index", scanPos + step, (int)(Compass.NumPosts.Y - 1), true, CTS.Token);
                Utility.Plotter.Collect = true;
                await Compass.ToStampPost("Index", scanPos + step, 0, true, CTS.Token, speed: Components.ScanParameters.ScanSpeed);
                Utility.Plotter.Collect = false;
                Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.toolStripProgressBar.PerformStep(); });

                scanPos += step * 2.0f;
            }

            Components.FormMain.BeginInvoke((MethodInvoker)delegate () { Components.FormMain.toolStripProgressBar.Visible = false; });

            if (!CTS.IsCancellationRequested) Utility.Plotter.ExportData();
        }
    }
}
