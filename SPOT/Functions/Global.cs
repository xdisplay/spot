﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using XDatabase;

namespace SPOT.Functions
{
    public static class Global
    {
        public static CancellationTokenSource CTS = new CancellationTokenSource();

        public static bool PositionMemoryEditMode { get; set; } = false;

        #region FileIO

        public static string OpenFile(string title, string filter)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = title;
                openFileDialog.Filter = filter;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                    return openFileDialog.FileName;
            }
            return null;
        }

        public static string SaveFile(string title, string filter)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.RestoreDirectory = true;
                saveFileDialog.Title = title;
                saveFileDialog.Filter = filter;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    return saveFileDialog.FileName;
            }
            return null;
        }

        public static void WriteToStream(FileStream stream, string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            stream.Write(bytes, 0, bytes.Length);
        }

        public static string GetDT()
        {
            DateTime dt = DateTime.Now;
            return dt.ToString("yyyy-MM-dd H:mm:ss");
        }

        #endregion

        #region Barcodes

        public static async Task<StampGeometry> GetBarcode(string ID)
        {
            //SPOT_DataService.InitProcessDataConnectionString("sqlappx1.database.windows.net", "ProcessData", "ProcessData_User", "GaggleWishFelt21!");
            StampGeometry sg = await SPOT_DataService.GetStampGeometryBySerialNumber(ID);
            if (sg == null || sg.FailureMessage.Length > 0)
                return new StampGeometry();
            else
                return sg;
        }

        #endregion
    }
}
