﻿using Aerotech.A3200;
using MathNet.Spatial.Euclidean;
using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static SPOT.Functions.Global;
using static SPOT.Components;
using System.Diagnostics;

namespace SPOT.Functions
{
    public static class Stages
    {
        public const ushort AbsMove = 0;
        public const ushort IncMove = 1;
        public static double[] XYSpeed = new double[] { 0.2, 1.50, 15.00 };
        public static double[] ZSpeed = new double[] { 0.05, 0.10, 5.00 };
        public static Point StageRange = new Point(200, 200);
        public static readonly int XAxis = 0;
        public static readonly int YAxis = 1;
        public static readonly int ZAxis = 2;

        #region A3200 Direct Calls

        public static void Home()
        {
            string programName = @"C:\SPOT\Programs\HOMEALL.pgm";
            MotionController.GetProgram((int)TaskId.T01).Load(programName);
            MotionController.GetProgram((int)TaskId.T01).Start();
        }

        public static void Move(int Axis, double Dist, double Speed, ushort MoveType, int Timeout, CancellationToken token)
        {
            try
            {
                while (!token.IsCancellationRequested)
                {
                    switch (MoveType)
                    {
                        case AbsMove:
                            {
                                MotionController.MoveAbs(Axis, Dist, Speed);
                                break;
                            }
                        case IncMove:
                            {
                                MotionController.MoveInc(Axis, Dist, Speed);
                                break;
                            }
                    }
                    MotionController.WaitForMotionDone(Aerotech.A3200.Commands.WaitOption.InPosition, Axis, Timeout);
                    return;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public static void MovePair(double xDist, double yDist, double Speed, ushort MoveType, int Timeout, CancellationToken token)
        {
            try
            {
                while (!token.IsCancellationRequested)
                {
                    switch (MoveType)
                    {
                        case AbsMove:
                            {
                                MotionController.SetupAbsolute();
                                break;
                            }

                        case IncMove:
                            {
                                MotionController.SetupIncremental();
                                break;
                            }
                    }
                    MotionController.MotionLinear(XAxis, YAxis, new double[] { xDist, yDist }, Speed);
                    MotionController.WaitForMotionDoneInAxis(XAxis, YAxis, Aerotech.A3200.Commands.WaitOption.InPosition, Timeout);
                    return;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        #endregion

        public static async Task ToCenter()
        {
            if (Application.UseWaitCursor) return;
            using (var wc = new Utility.WaitCursor())
                await Task.Run(() => MovePair(StageRange.X / 2, StageRange.Y / 2, XYSpeed[2], AbsMove, 5000, CTS.Token));
        }

        public static async Task ToLoad()
        {
            if (Application.UseWaitCursor) return;
            using (var wc = new Utility.WaitCursor())
                await Task.Run(() => MovePair(StageRange.X / 2, 0, XYSpeed[2], AbsMove, 5000, CTS.Token));
        }

        public static async Task Shuttle(bool toSensor = true)
        {
            if (Application.UseWaitCursor) return;
            using (var wc = new Utility.WaitCursor())
                await Task.Run(() => MovePair(
                    ComponentHeightSensor.HeightSensePos.X * (toSensor ? 1: -1), 
                    ComponentHeightSensor.HeightSensePos.Y * (toSensor ? 1 : -1), 
                    XYSpeed[2], IncMove, 500, CTS.Token));
        }

        public static async Task FindSensorPlane(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                if (StatusReporter.Status.I == 0)
                {
                    // Go Up
                    MotionController.FreeRun(ZAxis, 1 * ZSpeed[2]);
                    while (StatusReporter.Status.Z < ComponentHeightSensor.SafeZHeight)
                    {
                        Application.DoEvents();
                        if (StatusReporter.Status.I > 0)
                            break;
                    }
                    MotionController.FreeRunStop(ZAxis);

                    // Go Down
                    MotionController.FreeRun(ZAxis, -1 * ZSpeed[1]);
                    while (true)
                    {
                        Application.DoEvents();
                        if (StatusReporter.Status.I > 0)
                            break;
                    }
                    MotionController.FreeRunStop(ZAxis);
                }
                await Task.Run(() => token.WaitHandle.WaitOne(TimeSpan.FromSeconds(0.5)));
                await Task.Run(() => Move(ZAxis, ComponentHeightSensor.MidPoint - StatusReporter.Status.H, ZSpeed[0], IncMove, 10000, CTS.Token));
                return;
            }
        }

        public static async void MoveTo3D(Point3D point, CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                if (Application.UseWaitCursor) return;
                using (var wc = new Utility.WaitCursor())
                {
                    await Task.Run(() => MovePair(point.X, point.Y, XYSpeed[2], AbsMove, 10000, CTS.Token));
                    await Task.Run(() => Move(ZAxis, point.Z, ZSpeed[2], AbsMove, 10000, CTS.Token));
                }
                return;
            }
        }
    }
}
