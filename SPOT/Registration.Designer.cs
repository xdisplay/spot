﻿
namespace SPOT
{
    partial class Registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpRegisterStamp = new System.Windows.Forms.TableLayoutPanel();
            this.numDevicesY = new System.Windows.Forms.NumericUpDown();
            this.numDevicesX = new System.Windows.Forms.NumericUpDown();
            this.numPitchY = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numPitchX = new System.Windows.Forms.NumericUpDown();
            this.btnSaveSE = new System.Windows.Forms.Button();
            this.btnSaveSW = new System.Windows.Forms.Button();
            this.btnDone = new System.Windows.Forms.Button();
            this.tlpRegisterStamp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDevicesY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDevicesX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchX)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpRegisterStamp
            // 
            this.tlpRegisterStamp.ColumnCount = 3;
            this.tlpRegisterStamp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRegisterStamp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpRegisterStamp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpRegisterStamp.Controls.Add(this.numDevicesY, 2, 2);
            this.tlpRegisterStamp.Controls.Add(this.numDevicesX, 1, 2);
            this.tlpRegisterStamp.Controls.Add(this.numPitchY, 2, 1);
            this.tlpRegisterStamp.Controls.Add(this.label4, 0, 1);
            this.tlpRegisterStamp.Controls.Add(this.label3, 2, 0);
            this.tlpRegisterStamp.Controls.Add(this.label2, 1, 0);
            this.tlpRegisterStamp.Controls.Add(this.label5, 0, 2);
            this.tlpRegisterStamp.Controls.Add(this.numPitchX, 1, 1);
            this.tlpRegisterStamp.Controls.Add(this.btnSaveSE, 2, 4);
            this.tlpRegisterStamp.Controls.Add(this.btnSaveSW, 1, 4);
            this.tlpRegisterStamp.Controls.Add(this.btnDone, 0, 4);
            this.tlpRegisterStamp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpRegisterStamp.Location = new System.Drawing.Point(0, 0);
            this.tlpRegisterStamp.Name = "tlpRegisterStamp";
            this.tlpRegisterStamp.RowCount = 6;
            this.tlpRegisterStamp.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpRegisterStamp.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpRegisterStamp.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpRegisterStamp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tlpRegisterStamp.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpRegisterStamp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRegisterStamp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpRegisterStamp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpRegisterStamp.Size = new System.Drawing.Size(243, 111);
            this.tlpRegisterStamp.TabIndex = 0;
            // 
            // numDevicesY
            // 
            this.numDevicesY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numDevicesY.Location = new System.Drawing.Point(165, 42);
            this.numDevicesY.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numDevicesY.Name = "numDevicesY";
            this.numDevicesY.Size = new System.Drawing.Size(75, 20);
            this.numDevicesY.TabIndex = 9;
            this.numDevicesY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numDevicesX
            // 
            this.numDevicesX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numDevicesX.Location = new System.Drawing.Point(84, 42);
            this.numDevicesX.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numDevicesX.Name = "numDevicesX";
            this.numDevicesX.Size = new System.Drawing.Size(75, 20);
            this.numDevicesX.TabIndex = 8;
            this.numDevicesX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numPitchY
            // 
            this.numPitchY.DecimalPlaces = 2;
            this.numPitchY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numPitchY.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numPitchY.Location = new System.Drawing.Point(165, 16);
            this.numPitchY.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numPitchY.Name = "numPitchY";
            this.numPitchY.Size = new System.Drawing.Size(75, 20);
            this.numPitchY.TabIndex = 7;
            this.numPitchY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 26);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pitch (μm)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(165, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Y";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(84, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "X";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 26);
            this.label5.TabIndex = 4;
            this.label5.Text = "Device Count";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numPitchX
            // 
            this.numPitchX.DecimalPlaces = 2;
            this.numPitchX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numPitchX.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numPitchX.Location = new System.Drawing.Point(84, 16);
            this.numPitchX.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numPitchX.Name = "numPitchX";
            this.numPitchX.Size = new System.Drawing.Size(75, 20);
            this.numPitchX.TabIndex = 6;
            this.numPitchX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSaveSE
            // 
            this.btnSaveSE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveSE.BackColor = System.Drawing.Color.LightCoral;
            this.btnSaveSE.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnSaveSE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveSE.Location = new System.Drawing.Point(165, 78);
            this.btnSaveSE.Name = "btnSaveSE";
            this.btnSaveSE.Size = new System.Drawing.Size(75, 23);
            this.btnSaveSE.TabIndex = 13;
            this.btnSaveSE.Text = "Save SE";
            this.btnSaveSE.UseVisualStyleBackColor = false;
            this.btnSaveSE.Click += new System.EventHandler(this.btnSaveSE_Click);
            // 
            // btnSaveSW
            // 
            this.btnSaveSW.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveSW.BackColor = System.Drawing.Color.LightCoral;
            this.btnSaveSW.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnSaveSW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveSW.Location = new System.Drawing.Point(84, 78);
            this.btnSaveSW.Name = "btnSaveSW";
            this.btnSaveSW.Size = new System.Drawing.Size(75, 23);
            this.btnSaveSW.TabIndex = 12;
            this.btnSaveSW.Text = "Save SW";
            this.btnSaveSW.UseVisualStyleBackColor = false;
            this.btnSaveSW.Click += new System.EventHandler(this.btnSaveSW_Click);
            // 
            // btnDone
            // 
            this.btnDone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDone.BackColor = System.Drawing.Color.LightGreen;
            this.btnDone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDone.Location = new System.Drawing.Point(3, 78);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(75, 23);
            this.btnDone.TabIndex = 16;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = false;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // Registration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(243, 111);
            this.Controls.Add(this.tlpRegisterStamp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimumSize = new System.Drawing.Size(259, 150);
            this.Name = "Registration";
            this.Text = "Registration";
            this.TopMost = true;
            this.tlpRegisterStamp.ResumeLayout(false);
            this.tlpRegisterStamp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDevicesY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDevicesX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpRegisterStamp;
        private System.Windows.Forms.NumericUpDown numDevicesY;
        private System.Windows.Forms.NumericUpDown numDevicesX;
        private System.Windows.Forms.NumericUpDown numPitchY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numPitchX;
        private System.Windows.Forms.Button btnSaveSE;
        private System.Windows.Forms.Button btnSaveSW;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnDone;
    }
}