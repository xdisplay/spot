﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace SPOT
{
    public partial class Motion : UserControl
    {

        // UserControl overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Motion));
            this.cbxFreeRun = new System.Windows.Forms.CheckBox();
            this.lblY = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.tlpSpeedButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btnSlow = new System.Windows.Forms.Button();
            this.btnMedium = new System.Windows.Forms.Button();
            this.btnFast = new System.Windows.Forms.Button();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btnSouthEast = new System.Windows.Forms.Button();
            this.btnSouth = new System.Windows.Forms.Button();
            this.btnNorth = new System.Windows.Forms.Button();
            this.btnSouthWest = new System.Windows.Forms.Button();
            this.btnEast = new System.Windows.Forms.Button();
            this.btnWest = new System.Windows.Forms.Button();
            this.btnNorthEast = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnNorthWest = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnToZero = new System.Windows.Forms.Button();
            this.btnPosMem1 = new System.Windows.Forms.Button();
            this.btnPosMem2 = new System.Windows.Forms.Button();
            this.btnPosMem3 = new System.Windows.Forms.Button();
            this.numXincrement = new System.Windows.Forms.NumericUpDown();
            this.numYincrement = new System.Windows.Forms.NumericUpDown();
            this.numZincrement = new System.Windows.Forms.NumericUpDown();
            this.lblZ = new System.Windows.Forms.Label();
            this.tlpIncrement = new System.Windows.Forms.TableLayoutPanel();
            this.tlpSpeedButtons.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numXincrement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numYincrement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZincrement)).BeginInit();
            this.tlpIncrement.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxFreeRun
            // 
            this.cbxFreeRun.AutoSize = true;
            this.cbxFreeRun.Checked = true;
            this.cbxFreeRun.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxFreeRun.Location = new System.Drawing.Point(147, 168);
            this.cbxFreeRun.Name = "cbxFreeRun";
            this.cbxFreeRun.Size = new System.Drawing.Size(78, 21);
            this.cbxFreeRun.TabIndex = 19;
            this.cbxFreeRun.Text = "Free Run";
            this.cbxFreeRun.UseVisualStyleBackColor = true;
            this.cbxFreeRun.CheckedChanged += new System.EventHandler(this.cbxFreeRun_CheckedChanged);
            // 
            // lblY
            // 
            this.lblY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblY.Location = new System.Drawing.Point(91, 0);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(82, 17);
            this.lblY.TabIndex = 18;
            this.lblY.Text = "Y (µm)";
            this.lblY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblX
            // 
            this.lblX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblX.Location = new System.Drawing.Point(3, 0);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(82, 17);
            this.lblX.TabIndex = 17;
            this.lblX.Text = "X (µm)";
            this.lblX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tlpSpeedButtons
            // 
            this.tlpSpeedButtons.ColumnCount = 3;
            this.tlpSpeedButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpSpeedButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpSpeedButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpSpeedButtons.Controls.Add(this.btnSlow, 0, 0);
            this.tlpSpeedButtons.Controls.Add(this.btnMedium, 1, 0);
            this.tlpSpeedButtons.Controls.Add(this.btnFast, 2, 0);
            this.tlpSpeedButtons.Location = new System.Drawing.Point(40, 160);
            this.tlpSpeedButtons.Name = "tlpSpeedButtons";
            this.tlpSpeedButtons.RowCount = 1;
            this.tlpSpeedButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSpeedButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tlpSpeedButtons.Size = new System.Drawing.Size(97, 35);
            this.tlpSpeedButtons.TabIndex = 12;
            // 
            // btnSlow
            // 
            this.btnSlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSlow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSlow.Location = new System.Drawing.Point(3, 3);
            this.btnSlow.Name = "btnSlow";
            this.btnSlow.Size = new System.Drawing.Size(26, 29);
            this.btnSlow.TabIndex = 0;
            this.btnSlow.Tag = "0";
            this.btnSlow.Text = "S";
            this.btnSlow.UseVisualStyleBackColor = true;
            this.btnSlow.Click += new System.EventHandler(this.btnSpeed_Click);
            // 
            // btnMedium
            // 
            this.btnMedium.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMedium.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMedium.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMedium.Location = new System.Drawing.Point(35, 3);
            this.btnMedium.Name = "btnMedium";
            this.btnMedium.Size = new System.Drawing.Size(26, 29);
            this.btnMedium.TabIndex = 1;
            this.btnMedium.Tag = "1";
            this.btnMedium.Text = "M";
            this.btnMedium.UseVisualStyleBackColor = true;
            this.btnMedium.Click += new System.EventHandler(this.btnSpeed_Click);
            // 
            // btnFast
            // 
            this.btnFast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFast.Font = new System.Drawing.Font("Segoe UI", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFast.Location = new System.Drawing.Point(67, 3);
            this.btnFast.Name = "btnFast";
            this.btnFast.Size = new System.Drawing.Size(27, 29);
            this.btnFast.TabIndex = 2;
            this.btnFast.Tag = "2";
            this.btnFast.Text = "F";
            this.btnFast.UseVisualStyleBackColor = true;
            this.btnFast.Click += new System.EventHandler(this.btnSpeed_Click);
            // 
            // tlpButtons
            // 
            this.tlpButtons.ColumnCount = 5;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpButtons.Controls.Add(this.btnSouthEast, 2, 2);
            this.tlpButtons.Controls.Add(this.btnSouth, 1, 2);
            this.tlpButtons.Controls.Add(this.btnNorth, 1, 0);
            this.tlpButtons.Controls.Add(this.btnSouthWest, 0, 2);
            this.tlpButtons.Controls.Add(this.btnEast, 2, 1);
            this.tlpButtons.Controls.Add(this.btnWest, 0, 1);
            this.tlpButtons.Controls.Add(this.btnNorthEast, 2, 0);
            this.tlpButtons.Controls.Add(this.btnStop, 1, 1);
            this.tlpButtons.Controls.Add(this.btnNorthWest, 0, 0);
            this.tlpButtons.Controls.Add(this.btnUp, 4, 0);
            this.tlpButtons.Controls.Add(this.btnDown, 4, 2);
            this.tlpButtons.Controls.Add(this.btnToZero, 4, 1);
            this.tlpButtons.Controls.Add(this.btnPosMem1, 3, 0);
            this.tlpButtons.Controls.Add(this.btnPosMem2, 3, 1);
            this.tlpButtons.Controls.Add(this.btnPosMem3, 3, 2);
            this.tlpButtons.Location = new System.Drawing.Point(0, 0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 3;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpButtons.Size = new System.Drawing.Size(265, 157);
            this.tlpButtons.TabIndex = 11;
            // 
            // btnSouthEast
            // 
            this.btnSouthEast.BackColor = System.Drawing.SystemColors.Control;
            this.btnSouthEast.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSouthEast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSouthEast.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_se_20_32;
            this.btnSouthEast.Location = new System.Drawing.Point(109, 107);
            this.btnSouthEast.Name = "btnSouthEast";
            this.btnSouthEast.Size = new System.Drawing.Size(47, 47);
            this.btnSouthEast.TabIndex = 2;
            this.btnSouthEast.Tag = "3";
            this.btnSouthEast.UseVisualStyleBackColor = false;
            // 
            // btnSouth
            // 
            this.btnSouth.BackColor = System.Drawing.SystemColors.Control;
            this.btnSouth.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSouth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSouth.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_s_20_32;
            this.btnSouth.Location = new System.Drawing.Point(56, 107);
            this.btnSouth.Name = "btnSouth";
            this.btnSouth.Size = new System.Drawing.Size(47, 47);
            this.btnSouth.TabIndex = 2;
            this.btnSouth.Tag = "4";
            this.btnSouth.UseVisualStyleBackColor = false;
            // 
            // btnNorth
            // 
            this.btnNorth.BackColor = System.Drawing.SystemColors.Control;
            this.btnNorth.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnNorth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNorth.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_n_20_32;
            this.btnNorth.Location = new System.Drawing.Point(56, 3);
            this.btnNorth.Name = "btnNorth";
            this.btnNorth.Size = new System.Drawing.Size(47, 45);
            this.btnNorth.TabIndex = 2;
            this.btnNorth.Tag = "0";
            this.btnNorth.UseVisualStyleBackColor = false;
            // 
            // btnSouthWest
            // 
            this.btnSouthWest.BackColor = System.Drawing.SystemColors.Control;
            this.btnSouthWest.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSouthWest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSouthWest.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_sw_20_32;
            this.btnSouthWest.Location = new System.Drawing.Point(3, 107);
            this.btnSouthWest.Name = "btnSouthWest";
            this.btnSouthWest.Size = new System.Drawing.Size(47, 47);
            this.btnSouthWest.TabIndex = 2;
            this.btnSouthWest.Tag = "5";
            this.btnSouthWest.UseVisualStyleBackColor = false;
            // 
            // btnEast
            // 
            this.btnEast.BackColor = System.Drawing.SystemColors.Control;
            this.btnEast.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnEast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEast.Image = ((System.Drawing.Image)(resources.GetObject("btnEast.Image")));
            this.btnEast.Location = new System.Drawing.Point(109, 55);
            this.btnEast.Name = "btnEast";
            this.btnEast.Size = new System.Drawing.Size(47, 46);
            this.btnEast.TabIndex = 2;
            this.btnEast.Tag = "2";
            this.btnEast.UseVisualStyleBackColor = false;
            // 
            // btnWest
            // 
            this.btnWest.BackColor = System.Drawing.SystemColors.Control;
            this.btnWest.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnWest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWest.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_w_20_32;
            this.btnWest.Location = new System.Drawing.Point(3, 55);
            this.btnWest.Name = "btnWest";
            this.btnWest.Size = new System.Drawing.Size(47, 46);
            this.btnWest.TabIndex = 2;
            this.btnWest.Tag = "6";
            this.btnWest.UseVisualStyleBackColor = false;
            // 
            // btnNorthEast
            // 
            this.btnNorthEast.BackColor = System.Drawing.SystemColors.Control;
            this.btnNorthEast.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnNorthEast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNorthEast.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_ne_20_32;
            this.btnNorthEast.Location = new System.Drawing.Point(109, 3);
            this.btnNorthEast.Name = "btnNorthEast";
            this.btnNorthEast.Size = new System.Drawing.Size(47, 45);
            this.btnNorthEast.TabIndex = 2;
            this.btnNorthEast.Tag = "1";
            this.btnNorthEast.UseVisualStyleBackColor = false;
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnStop.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnStop.Image = global::SPOT.Properties.Resources.iconmonstr_stop_5_32;
            this.btnStop.Location = new System.Drawing.Point(56, 55);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(47, 46);
            this.btnStop.TabIndex = 2;
            this.btnStop.Tag = "10";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnNorthWest
            // 
            this.btnNorthWest.BackColor = System.Drawing.SystemColors.Control;
            this.btnNorthWest.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnNorthWest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNorthWest.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_nw_20_32;
            this.btnNorthWest.Location = new System.Drawing.Point(3, 3);
            this.btnNorthWest.Name = "btnNorthWest";
            this.btnNorthWest.Size = new System.Drawing.Size(47, 45);
            this.btnNorthWest.TabIndex = 0;
            this.btnNorthWest.Tag = "7";
            this.btnNorthWest.UseVisualStyleBackColor = false;
            // 
            // btnUp
            // 
            this.btnUp.BackColor = System.Drawing.SystemColors.Control;
            this.btnUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUp.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUp.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_n_24_24;
            this.btnUp.Location = new System.Drawing.Point(212, 3);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(50, 46);
            this.btnUp.TabIndex = 3;
            this.btnUp.Tag = "8";
            this.btnUp.UseVisualStyleBackColor = false;
            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.SystemColors.Control;
            this.btnDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDown.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDown.Image = global::SPOT.Properties.Resources.iconmonstr_arrow_s_24_24;
            this.btnDown.Location = new System.Drawing.Point(212, 107);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(50, 47);
            this.btnDown.TabIndex = 4;
            this.btnDown.Tag = "9";
            this.btnDown.UseVisualStyleBackColor = false;
            // 
            // btnToZero
            // 
            this.btnToZero.BackColor = System.Drawing.Color.LawnGreen;
            this.btnToZero.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnToZero.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnToZero.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToZero.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToZero.Location = new System.Drawing.Point(212, 55);
            this.btnToZero.Name = "btnToZero";
            this.btnToZero.Size = new System.Drawing.Size(50, 46);
            this.btnToZero.TabIndex = 5;
            this.btnToZero.Tag = "10";
            this.btnToZero.Text = "To Zero";
            this.btnToZero.UseVisualStyleBackColor = false;
            this.btnToZero.Click += new System.EventHandler(this.btnToZero_Click);
            // 
            // btnPosMem1
            // 
            this.btnPosMem1.BackColor = System.Drawing.Color.White;
            this.btnPosMem1.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnPosMem1.FlatAppearance.BorderSize = 4;
            this.btnPosMem1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosMem1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPosMem1.Location = new System.Drawing.Point(162, 3);
            this.btnPosMem1.Name = "btnPosMem1";
            this.btnPosMem1.Size = new System.Drawing.Size(44, 46);
            this.btnPosMem1.TabIndex = 6;
            this.btnPosMem1.Tag = "10";
            this.btnPosMem1.Text = "1";
            this.btnPosMem1.UseVisualStyleBackColor = false;
            // 
            // btnPosMem2
            // 
            this.btnPosMem2.BackColor = System.Drawing.Color.White;
            this.btnPosMem2.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnPosMem2.FlatAppearance.BorderSize = 4;
            this.btnPosMem2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosMem2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPosMem2.Location = new System.Drawing.Point(162, 55);
            this.btnPosMem2.Name = "btnPosMem2";
            this.btnPosMem2.Size = new System.Drawing.Size(44, 46);
            this.btnPosMem2.TabIndex = 7;
            this.btnPosMem2.Tag = "10";
            this.btnPosMem2.Text = "2";
            this.btnPosMem2.UseVisualStyleBackColor = false;
            // 
            // btnPosMem3
            // 
            this.btnPosMem3.BackColor = System.Drawing.Color.White;
            this.btnPosMem3.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnPosMem3.FlatAppearance.BorderSize = 4;
            this.btnPosMem3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosMem3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPosMem3.Location = new System.Drawing.Point(162, 107);
            this.btnPosMem3.Name = "btnPosMem3";
            this.btnPosMem3.Size = new System.Drawing.Size(44, 46);
            this.btnPosMem3.TabIndex = 8;
            this.btnPosMem3.Tag = "10";
            this.btnPosMem3.Text = "3";
            this.btnPosMem3.UseVisualStyleBackColor = false;
            // 
            // numXincrement
            // 
            this.numXincrement.DecimalPlaces = 1;
            this.numXincrement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numXincrement.Location = new System.Drawing.Point(3, 20);
            this.numXincrement.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numXincrement.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numXincrement.Name = "numXincrement";
            this.numXincrement.Size = new System.Drawing.Size(82, 25);
            this.numXincrement.TabIndex = 22;
            this.numXincrement.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numXincrement.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numYincrement
            // 
            this.numYincrement.DecimalPlaces = 1;
            this.numYincrement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numYincrement.Location = new System.Drawing.Point(91, 20);
            this.numYincrement.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numYincrement.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numYincrement.Name = "numYincrement";
            this.numYincrement.Size = new System.Drawing.Size(82, 25);
            this.numYincrement.TabIndex = 23;
            this.numYincrement.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numYincrement.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numZincrement
            // 
            this.numZincrement.DecimalPlaces = 1;
            this.numZincrement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numZincrement.Location = new System.Drawing.Point(179, 20);
            this.numZincrement.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numZincrement.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numZincrement.Name = "numZincrement";
            this.numZincrement.Size = new System.Drawing.Size(83, 25);
            this.numZincrement.TabIndex = 25;
            this.numZincrement.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numZincrement.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lblZ
            // 
            this.lblZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblZ.Location = new System.Drawing.Point(179, 0);
            this.lblZ.Name = "lblZ";
            this.lblZ.Size = new System.Drawing.Size(83, 17);
            this.lblZ.TabIndex = 24;
            this.lblZ.Text = "Z (µm)";
            this.lblZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tlpIncrement
            // 
            this.tlpIncrement.ColumnCount = 3;
            this.tlpIncrement.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpIncrement.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpIncrement.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpIncrement.Controls.Add(this.lblX, 0, 0);
            this.tlpIncrement.Controls.Add(this.numZincrement, 2, 1);
            this.tlpIncrement.Controls.Add(this.lblY, 1, 0);
            this.tlpIncrement.Controls.Add(this.numYincrement, 1, 1);
            this.tlpIncrement.Controls.Add(this.lblZ, 2, 0);
            this.tlpIncrement.Controls.Add(this.numXincrement, 0, 1);
            this.tlpIncrement.Location = new System.Drawing.Point(0, 199);
            this.tlpIncrement.Name = "tlpIncrement";
            this.tlpIncrement.RowCount = 2;
            this.tlpIncrement.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpIncrement.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpIncrement.Size = new System.Drawing.Size(265, 50);
            this.tlpIncrement.TabIndex = 26;
            // 
            // Motion
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.tlpIncrement);
            this.Controls.Add(this.cbxFreeRun);
            this.Controls.Add(this.tlpSpeedButtons);
            this.Controls.Add(this.tlpButtons);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Motion";
            this.Size = new System.Drawing.Size(265, 247);
            this.Load += new System.EventHandler(this.XYMotion_Load);
            this.tlpSpeedButtons.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numXincrement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numYincrement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZincrement)).EndInit();
            this.tlpIncrement.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private CheckBox cbxFreeRun;
        private Label lblY;
        private Label lblX;
        private TableLayoutPanel tlpSpeedButtons;
        private Button btnSlow;
        private Button btnMedium;
        private Button btnFast;
        private TableLayoutPanel tlpButtons;
        private Button btnSouthEast;
        private Button btnSouth;
        private Button btnNorth;
        private Button btnSouthWest;
        private Button btnEast;
        private Button btnWest;
        private Button btnNorthEast;
        private Button btnStop;
        private Button btnNorthWest;
        private Button btnUp;
        private Button btnDown;
        private Label lblZ;
        private TableLayoutPanel tlpIncrement;
        private Button btnToZero;
        public Button btnPosMem1;
        public Button btnPosMem2;
        public Button btnPosMem3;
        public NumericUpDown numXincrement;
        public NumericUpDown numYincrement;
        public NumericUpDown numZincrement;
    }
}