﻿using Aerotech.A3200;
using Aerotech.A3200.Tasks;
using AForge.Video;
using AForge.Video.DirectShow;
using MicroEpsilon;
using OxyPlot.WindowsForms;
using SPOT.Interfaces;
using SPOT.Utility;
using System;
using System.Drawing;
using System.Windows.Forms;
using XDatabase;
using AdvancedIllumination;
using static SPOT.Components;
using static SPOT.Functions.Global;
using AVT.VmbAPINET;
using System.Drawing.Imaging;

namespace SPOT
{
    public static class Components
    {
        public static bool DUMMY = false;
        public static bool ConfocalDovetail = true;
        public static FormMain FormMain = null;
        public static bool Zoom = false;
        public static Motion ucMotion = new Motion();
        public static ScanParameters ScanParameters = new ScanParameters();
        public static PictureBox Vision = null;
        public static PlotView PlotView = null;
        public static PositionMemory PositionMemory = null;
        public static VideoCaptureDevice VideoSource = null;
        public static StatusTracker Provider = new StatusTracker();
        public static StatusReporter Reporter = new StatusReporter("SPOT");
        public static MEDAQLib HeightSensor = null;
        public static IController MotionController = null;
        public static DCS_100 LEDController = null;
        public static SEYR.Session.Channel SEYRCh = null;
        public static Bitmap LastImage = null;
        public static GridMaker.Composer GridComposer = new GridMaker.Composer();
        public static SEYRpanel SEYRpanel = null;

        public static void Setup(FormMain formMain)
        {
            Application.ApplicationExit += ApplicationExit;

            FormMain = formMain;

            FormMain.tlpMain.Controls.Add(ucMotion, 0, 1);
            FormMain.tlpMain.SetColumnSpan(ucMotion, 3);

            Vision = FormMain.pbxVision;
            Vision.MouseDown += ComponentCamera.ToggleCrosshair;

            PlotView = FormMain.plotView;
            Plotter.InitializePlot();
            PositionMemory = new PositionMemory();

            if (!DUMMY)
            {
                System.Threading.Tasks.Task.Run(() => SPOT_DataService.InitProcessDataConnectionString("sqlappx1.database.windows.net", "ProcessData", "ProcessData_User", "GaggleWishFelt21!"));
                THUM.SetTempUnit(1); // °C
                ComponentCamera.Initialize();
                ComponentHeightSensor.Initialize();
                ComponentA3200.Initialize();
                ComponentLEDController.Initialize(FormMain.NumWhiteLED.Value, FormMain.NumRedLED.Value);
            }
        }

        public static void Reset()
        {
            if (!DUMMY)
            {
                Compass.MovingToPost = false;
                MotionController.MotionFaultAck(AxisMask.All);
                MotionController.AcknowledgeAll();
                ComponentA3200.ReInitialize();
            }
            CTS = new System.Threading.CancellationTokenSource();
            FormMain.toolStripButtonReset.BackColor = SystemColors.Control;
        }

        private static void ApplicationExit(object sender, EventArgs e)
        {
            if (!DUMMY)
            {
                if (VideoSource.IsRunning)
                {
                    VideoSource.SignalToStop();
                    VideoSource.WaitForStop();
                }
                MotionController.DiagnosticsPacketArrived -= PacketHandler.NewDiagPacketArrived;
                MotionController.AbortMotion(AxisMask.All);
                ComponentLEDController.Disconnect();
                ComponentCamera.Disconnect();
                ComponentVimba.Disconnect();
            }
        }
    }

    public partial class ComponentCamera
    {
        private static bool ShowCrosshair = false;
        private static readonly string Scope = "@device:pnp:\\\\?\\usb#vid_0c45&pid_6369&mi_00#6&260c6350&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\\global";

        public static void Initialize()
        {
            FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            VideoSource = new VideoCaptureDevice(Scope);
            VideoSource.NewFrame += new NewFrameEventHandler(Video_NewFrame);
            VideoSource.Start();
        }

        public static void Disconnect()
        {
            if (VideoSource != null)
            {
                VideoSource.SignalToStop();
                VideoSource.WaitForStop();
            }
        }

        private static void Video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            if (SEYRCh != null) LastImage = (Bitmap)eventArgs.Frame.Clone();
            Bitmap img = (Bitmap)eventArgs.Frame.Clone();
            Vision.BackgroundImage = Zoom ? ZoomImage.NewFrame(img) : img;
        }

        public static void ToggleCrosshair(object sender, MouseEventArgs e)
        {
            try
            {
                ShowCrosshair = !ShowCrosshair;

                Bitmap bitmap = new Bitmap(Vision.BackgroundImage.Width, Vision.BackgroundImage.Height);
                if (ShowCrosshair)
                {
                    Pen pen = new Pen(Color.FromArgb(100, Color.LawnGreen), bitmap.Height / 150);
                    Graphics g = Graphics.FromImage(bitmap);
                    g.DrawLine(pen, new Point(bitmap.Width / 2, 0), new Point(bitmap.Width / 2, bitmap.Height));
                    g.DrawLine(pen, new Point(0, bitmap.Height / 2), new Point(bitmap.Width, bitmap.Height / 2));
                }
                Vision.Image = bitmap;
            }
            catch (Exception) { }
        }
    }

    public partial class ComponentHeightSensor
    {
        public static double MidPoint = 2.00;
        public static double SafeZHeight = -25.0;
        public static PointF HeightSensePos = new PointF(46.847f, 2.204f);

        public static void Initialize()
        {
            // Open Height Sensor and check for error
            HeightSensor = new MEDAQLib("IFC2422");
            if (Open(ref HeightSensor) != ERR_CODE.ERR_NOERROR)
                return;
        }

        public static ERR_CODE Open(ref MEDAQLib sensor)
        {
            if (sensor.OpenSensorTCPIP("192.168.1.15") != ERR_CODE.ERR_NOERROR)
            {
                string errText = "";
                ERR_CODE err = sensor.GetError(ref errText);
                return err;
            }
            return ERR_CODE.ERR_NOERROR;
        }

        public static string GetDetectionThreshold()
        {
            if (HeightSensor == null) return string.Empty;
            return $"{GetParamDbl("MinimumThreshold_Ch1")}";
        }

        private static string GetParamDbl(string key)
        {
            if (HeightSensor.ExecSCmd($"Get_{key}") != ERR_CODE.ERR_NOERROR)
            {
                string errText = "";
                _ = HeightSensor.GetError(ref errText);
                return errText;
            }
            var value = default(double);
            if (HeightSensor.GetParameterDouble($"SA_{key}", ref value) != ERR_CODE.ERR_NOERROR)
            {
                string errText = "";
                _ = HeightSensor.GetError(ref errText);
                return errText;
            }
            return value.ToString();
        }
    }

    public partial class ComponentA3200
    {
        public static void Initialize()
        {
            MotionController = new A3000Controller();
            MotionController.Connect();
            MotionController.AbortMotion(AxisMask.All);
            MotionController.EnableMotion(AxisMask.All);
            MotionController.AcknowledgeAll();
            for (int i = 1; i <= 3; i++)
            {
                MotionController.GetTaskState(i);
                if (MotionController.GetTaskState(i) == TaskState.ProgramRunning)
                {
                    MotionController.StopProgram(i);
                }
            }
            MotionController.AcknowledgeAll();
            Reporter.Subscribe(Provider);
            MotionController.DiagnosticsPacketArrived += PacketHandler.NewDiagPacketArrived;
        }

        public static void ReInitialize()
        {
            MotionController.EnableMotion(AxisMask.All);
            MotionController.AcknowledgeAll();
            for (int i = 1; i <= 3; i++)
            {
                MotionController.GetTaskState(i);
                if (MotionController.GetTaskState(i) == TaskState.ProgramRunning)
                {
                    MotionController.StopProgram(i);
                }
            }
            MotionController.AcknowledgeAll();
        }
    }

    public partial class ComponentLEDController
    {
        public static void Initialize(decimal white, decimal red)
        {
            LEDController = new DCS_100("192.168.1.10");
            LEDController.Connect();
            LEDController.SetMode((DCS_100.Channel)2, DCS_100.Mode.Continuous);
            LEDController.SetCurrent((DCS_100.Channel)2, (ushort)white);
            LEDController.SetMode((DCS_100.Channel)3, DCS_100.Mode.Continuous);
            LEDController.SetCurrent((DCS_100.Channel)3, (ushort)red);
        }

        public static void Disconnect()
        {
            if (LEDController != null)
            {
                LEDController.SetMode((DCS_100.Channel)2, DCS_100.Mode.Off);
                LEDController.SetMode((DCS_100.Channel)3, DCS_100.Mode.Off);
                LEDController.Disconnect();
            }
        }

        public static void Set(int ch, decimal value)
        {
            if (LEDController == null) return;
            LEDController.SetCurrent((DCS_100.Channel)ch, (ushort)value);
        }
    }

    public partial class ComponentVimba
    {
        private static Vimba VimbaSystem = null;
        private static Camera Camera = null;

        public static void Initialize()
        {
            VimbaSystem = new Vimba();
            VimbaSystem.Startup();
            try
            {
                Camera = VimbaSystem.OpenCameraByID("192.168.21.201", VmbAccessModeType.VmbAccessModeFull);
                Camera.OnFrameReceived += new Camera.OnFrameReceivedHandler(OnFrameReceived);

                FeatureCollection features = Camera.Features;
                Feature feature = features["PayloadSize"];
                long payloadSize = feature.IntValue;
                Frame[] frameArray = new Frame[3];

                for (int index = 0; index < frameArray.Length; ++index)
                {
                    frameArray[index] = new Frame(payloadSize);
                    Camera.AnnounceFrame(frameArray[index]);
                }

                Camera.StartCapture();

                for (int index = 0; index < frameArray.Length; ++index)
                {
                    Camera.QueueFrame(frameArray[index]);
                }

                feature = features["AcquisitionMode"];
                feature.EnumValue = "Continuous";
                feature = features["AcquisitionStart"];
                feature.RunCommand();
            }
            catch (VimbaException ve)
            {
                System.Diagnostics.Debug.WriteLine("Camera open error : " + ve.MapReturnCodeToString());
            }
        }

        private static void StopCamera()
        {
            FeatureCollection features = Camera.Features;
            Feature feature = features["AcquisitionStop"];
            feature.RunCommand();
            Camera.EndCapture();
            Camera.FlushQueue();
            Camera.RevokeAllFrames();
            Camera.Close();
            Camera = null;
        }

        public static void Disconnect()
        {
            if (Camera != null)
            {
                StopCamera();
                VimbaSystem.Shutdown();
            }
        }

        private static void OnFrameReceived(Frame frame)
        {
            Camera.QueueFrame(frame);
            Bitmap bitmap = new Bitmap((int)frame.Width, (int)frame.Height, PixelFormat.Format24bppRgb);
            frame.Fill(ref bitmap);
            if (SEYRCh != null) LastImage = (Bitmap)bitmap.Clone();
            Bitmap img = (Bitmap)bitmap.Clone();
            Vision.BackgroundImage = Zoom ? ZoomImage.NewFrame(img) : img;
        }
    }
}
