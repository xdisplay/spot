﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Documentation
{
    public partial class FormMain : Form
    {
        private List<Node> NodeList = new List<Node>();

        public FormMain()
        {
            InitializeComponent();

            Node Setup = new Node("Setup", root: true);
            Setup.Nodes.Add(new Node("Opening Software"));
            Setup.Nodes.Add(new Node("Swapping Chucks"));
            Setup.Nodes.Add(new Node("Calibrate Sidecam"));
            NodeList.Add(Setup);

            Node Stamp = new Node("Stamp", root: true);
            Stamp.Nodes.Add(new Node("Find Stamp Plane"));
            Stamp.Nodes.Add(new Node("Register Stamp"));
            Stamp.Nodes.Add(new Node("Scan Tuning"));
            NodeList.Add(Stamp);

            Node Scanning = new Node("Scanning", root: true);
            Scanning.Nodes.Add(new Node("Custom Parameters"));
            NodeList.Add(Scanning);

            // Model is the currently queried object, we return true or false according to the amount of children we have in our Nodes List
            treeListView.CanExpandGetter = model => ((Node)model).Nodes.Count > 0;
            // We return the list of Branches that shall be considered Children.
            treeListView.ChildrenGetter = delegate (object model)
            {
                return ((Node)model).Nodes;
            };
            // We also need to tell OLV what objects to display as root nodes
            treeListView.SetObjects(NodeList);
            // Bind button click for nodes
            treeListView.ItemActivate += TreeListView_ItemActivate;

            Show();
        }

        private void TreeListView_ItemActivate(object sender, System.EventArgs e)
        {
            Node node = (Node)treeListView.SelectedObject;
            ToggleVideoButton(node);
            rtb.Text = node.Info;
        }

        private void ToggleVideoButton(Node node)
        {
            if (node.HasVideo)
            {
                btnOpenVideo.Enabled = true;
                btnOpenVideo.BackColor = Color.LightGreen;
            }
            else
            {
                btnOpenVideo.Enabled = false;
                btnOpenVideo.BackColor = SystemColors.Control;
            }
        }

        private void btnOpenVideo_Click(object sender, System.EventArgs e)
        {
            Node thisNode = (Node)treeListView.SelectedObject;
            OpenWithDefaultProgram(thisNode.VideoPath);
        }

        public static void OpenWithDefaultProgram(string path)
        {
            Process fileopener = new Process();
            fileopener.StartInfo.FileName = "explorer";
            fileopener.StartInfo.Arguments = "\"" + path + "\"";
            fileopener.Start();
        }
    }
}
