﻿Navigate the optics to the SW corner of the post array.

Turn on the crosshair by clicking the video feed.

Fine tune the position of the crosshair as best as possible with the Zoom form.

Open the Registration form and save the SW position.

Repeat the process for the SE position.

Enter the stamp information into the Registration form and press "Done".

Verify the registration by navigating around the stamp with the Compass form.