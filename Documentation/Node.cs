﻿using System.Collections.Generic;
using System.IO;

namespace Documentation
{
    public class Node
    {
        // Video Root Directory
        private readonly string VRD = @"\\XDISPLAY-FS01.x-display.local\Company\Systems Eng\SPOT Network Folder\VideoDocumentation\";

        public string Name { get; set; }
        public string Info {  get; set; } = string.Empty;
        public bool HasVideo {  get; set; } = false;
        public string VideoPath { get; set; } = string.Empty;
        public List<Node> Nodes { get; set; } = new List<Node>();
        public Node(string name, bool root = false)
        {
            Name = name;

            if (root) return;

            string id = Name.Replace(" ", "");

            string testInfo = string.Format(@"Info\{0}.txt", id);
            if (File.Exists(testInfo))
                Info = File.ReadAllText(testInfo);

            string testVideo = string.Format("{0}{1}.mp4", VRD, id);
            if (File.Exists(testVideo))
            {
                HasVideo = true;
                VideoPath = testVideo;
            }
        }
    }
}
